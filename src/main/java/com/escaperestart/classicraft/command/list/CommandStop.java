package com.escaperestart.classicraft.command.list;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.Util;

public class CommandStop extends Command {

    private static final Logger LOGGER = LogManager.getLogger();

    public CommandStop(Server server) {
	super(server, "stop", "Stops the server", "classicraft.command.stop", "end", "close");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	LOGGER.info("{} is stopping the server...", executor.getName());
	String reason = Util.stringJoint(" ", args);
	this.server.stop(reason.length() > 0 ? reason : null);
    }
}
