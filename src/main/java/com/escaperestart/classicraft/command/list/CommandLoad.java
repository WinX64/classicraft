package com.escaperestart.classicraft.command.list;

import java.io.File;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.world.World;
import com.escaperestart.classicraft.world.WorldCreator;

public class CommandLoad extends Command {

    private static final File WORLD_FOLDER = new File("C:\\Users\\Lucas\\Desktop\\new-levels");

    public CommandLoad(Server server) {
	super(server, "load", "Loads a level from a file", "classicraft.command.load", "loadlevel");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (args.length < 1) {
	    executor.sendMessage(String.format(ChatColor.RED + "Syntax: /%s <file>", alias));
	    return;
	}

	World world = server.getWorld(args[0]);
	if (world != null) {
	    executor.sendMessage(String.format(ChatColor.RED + "World \"%s\" already exists!", args[0].toLowerCase()));
	    return;
	}

	if (args[0].equals("*")) {
	    for (File file : WORLD_FOLDER.listFiles()) {
		if (!file.getName().endsWith(".lvl")) {
		    continue;
		}
		if (server.getWorld(file.getName().substring(0, file.getName().indexOf('.'))) != null) {
		    continue;
		}
		World newWorld = WorldCreator.loadWorldFromFile(server, file);
		if (newWorld != null) {
		    server.getWorlds().add(newWorld);
		    executor.sendMessage(String.format(ChatColor.GREEN + "World \"%s\" loaded!", newWorld.getName()));
		}
	    }
	    executor.sendMessage(ChatColor.DARK_GREEN + "All worlds loaded!");
	    return;
	}

	File worldFile = new File(WORLD_FOLDER, args[0] + ".lvl");
	if (!worldFile.exists()) {
	    executor.sendMessage(String.format(ChatColor.RED + "File \"%s\" does not exist!", args[0]));
	    return;
	}

	World newWorld = WorldCreator.loadWorldFromFile(server, new File(WORLD_FOLDER, args[0] + ".lvl"));
	if (newWorld == null) {
	    executor.sendMessage(ChatColor.RED + "An error occurred while trying to load the world from the disk!");
	    return;
	}

	server.getWorlds().add(newWorld);
	executor.sendMessage(String.format(ChatColor.GREEN + "World \"%s\" loaded!", newWorld.getName()));
    }
}
