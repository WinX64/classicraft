package com.escaperestart.classicraft.command.list;

import java.util.List;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class CommandOnline extends Command {

    public CommandOnline(Server server) {
	super(server, "online", "Displays the online players", "classicraft.command.online", "who", "players", "list");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	List<Player> players = server.getPlayers();
	StringBuilder builder = new StringBuilder();
	for (Player player : players) {
	    builder.append(ChatColor.WHITE + ", ").append(player.getColoredName());
	}
	String playersOnline = builder.length() > 0 ? builder.substring(4) : builder.toString();
	executor.sendMessage("Online players(" + players.size() + "): " + playersOnline);
    }
}
