package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.network.packet.out.PacketOutHackControl;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.util.Util;

public class CommandHacks extends Command {

    public CommandHacks(Server server) {
	super(server, "hacks", "Toggle the ability to use hacks", "classicraft.command.hacks", "hax", "hack");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (!(executor instanceof Player)) {
	    executor.sendMessage(ChatColor.RED + "This command cannot be used via console!");
	    return;
	}

	Player player = (Player) executor;
	boolean state = !player.hacksAllowed();
	player.setHacksAllowed(state);

	player.getClientConnection().sendPacket(new PacketOutHackControl(state, state, state, state, state, state ? 100 : -1), null);
	player.sendMessage(ChatColor.YELLOW + "Hacks allowed: " + Util.getState(state, true));
    }
}
