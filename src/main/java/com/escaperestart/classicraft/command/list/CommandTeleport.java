package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class CommandTeleport extends Command {

    public CommandTeleport(Server server) {
	super(server, "teleport", "Teleports to other players", "classicraft.command.teleport", "tp");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (!(executor instanceof Player)) {
	    executor.sendMessage(ChatColor.RED + "This command cannot be used via console!");
	    return;
	}

	if (args.length < 1) {
	    executor.sendMessage(String.format(ChatColor.RED + "Syntax: /%s <player>", alias));
	    return;
	}

	Player player = (Player) executor;
	Player target = server.getPlayer(args[0]);
	if (target == null) {
	    executor.sendMessage(String.format(ChatColor.RED + "Player \"%s\" could not be found!", args[0].toLowerCase()));
	    return;
	}

	player.teleport(target);
	executor.sendMessage(ChatColor.YELLOW + "Teleported!");
    }
}
