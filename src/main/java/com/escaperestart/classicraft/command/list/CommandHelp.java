package com.escaperestart.classicraft.command.list;

import java.util.Arrays;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class CommandHelp extends Command {

    public CommandHelp(Server server) {
	super(server, "help", "Shows information about the server", "classicraft.command.help", "?");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (args.length < 1) {
	    StringBuilder builder = new StringBuilder();
	    for (Command command : server.getCommandList().getCommands()) {
		builder.append(ChatColor.WHITE + ", ").append(ChatColor.GRAY).append(command.getName());
	    }
	    executor.sendMessage(ChatColor.RED + "Syntax: /help <command>");
	    executor.sendMessage(ChatColor.YELLOW + "Available commands: "
		    + (builder.length() > 0 ? builder.substring(4) : ChatColor.RED + "None"));
	} else {
	    Command command = server.getCommandList().getCommand(args[0]);
	    if (command == null) {
		executor.sendMessage(ChatColor.RED + "The specified command could not be found!");
		return;
	    }

	    StringBuilder builder = new StringBuilder();
	    builder.append(ChatColor.YELLOW + "Command ").append(command.getName()).append("\n");
	    builder.append(ChatColor.GRAY + "Description: " + ChatColor.WHITE).append(command.getDescription())
		    .append("\n");
	    builder.append(ChatColor.GRAY + "Aliases: " + ChatColor.WHITE).append(Arrays.toString(command.getAliases()))
		    .append("\n");
	    builder.append(ChatColor.GRAY + "Permission: " + ChatColor.WHITE).append(command.getPermission());
	    executor.sendMessage(builder.toString());
	}
    }
}
