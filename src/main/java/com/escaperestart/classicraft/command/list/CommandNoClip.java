package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.util.Util;

public class CommandNoClip extends Command {

    public CommandNoClip(Server server) {
	super(server, "noclip", "Toggle the ability to clip through blocks", "classicraft.command.noclip", "clip");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (!(executor instanceof Player)) {
	    executor.sendMessage(ChatColor.RED + "This command cannot be used via console!");
	    return;
	}

	Player player = (Player) executor;
	boolean state = !player.noClipAllowed();
	player.setNoClipAllowed(state);

	player.sendMessage(ChatColor.YELLOW + "NoClip allowed: " + Util.getState(state, true));
    }
}
