package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.permission.Rank;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class CommandPermissions extends Command {

    private static final String HELP = ChatColor.RED + "Syntax: /%s <rank> <set|unset|list> [...]";
    private static final String HELP_SET = ChatColor.RED + "Syntax: /%s %s set <permission> <value>";
    private static final String HELP_UNSET = ChatColor.RED + "Syntax: /%s %s unset <permission>";

    private static final String INVALID_RANK = ChatColor.RED + "Invalid rank!";
    private static final String AVAILABLE_RANKS = ChatColor.YELLOW + "Available ranks: ";

    private static final String INVALID_OPTION = ChatColor.RED + "Invalid option!";
    private static final String AVAILABLE_OPTIONS = ChatColor.YELLOW + "Available options: " + ChatColor.WHITE
	    + "set, unset, list";

    private static final String RANK_PERMISSIONS = ChatColor.YELLOW + "Permissions of the rank %s" + ChatColor.YELLOW + ":";

    public CommandPermissions(Server server) {
	super(server, "permissions", "Manage the different rank's permissions", "classicraft.command.permissions",
		"permission", "perms", "perm");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (args.length < 2) {
	    executor.sendMessage(String.format(HELP, alias));
	    return;
	}

	Rank targetRank = server.getPermissionManager().getRank(args[0]);
	if (targetRank == null) {
	    StringBuilder ranks = new StringBuilder();
	    for (Rank rank : server.getPermissionManager().getRanks()) {
		ranks.append(ChatColor.WHITE + ", ").append(rank.getColoredName());
	    }
	    String availableRanks = ranks.length() > 0 ? ranks.substring(4) : ChatColor.RED + "---";
	    executor.sendMessage(INVALID_RANK);
	    executor.sendMessage(AVAILABLE_RANKS + availableRanks);
	    return;
	}

	StringBuilder message = new StringBuilder();
	switch (args[1].toLowerCase()) {
	    case "set":
		if (args.length < 4) {
		    executor.sendMessage(String.format(HELP_SET, alias, targetRank.getName()));
		    return;
		}
		executor.sendMessage(ChatColor.RED + "TBA!");
		break;

	    case "unset":
		if (args.length < 4) {
		    executor.sendMessage(String.format(HELP_UNSET, alias, targetRank.getName()));
		    return;
		}
		executor.sendMessage(ChatColor.RED + "TBA!");
		break;

	    case "list":
		message.append(String.format(RANK_PERMISSIONS, targetRank.getColoredName()));
		for (String permission : targetRank.getPermissions()) {
		    message.append("\n" + ChatColor.GREEN).append(permission);
		}
		executor.sendMessage(message.toString());
		break;

	    default:
		executor.sendMessage(INVALID_OPTION);
		executor.sendMessage(AVAILABLE_OPTIONS);
		break;
	}
    }
}
