package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.world.World;

public class CommandMap extends Command {

    public CommandMap(Server server) {
	super(server, "map", "Displays info about a map", "classicraft.command.map", "mapinfo");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (args.length < 1) {
	    executor.sendMessage(String.format(ChatColor.RED + "Syntax: /%s <world>", alias));
	    return;
	}

	World world = server.getWorld(args[0]);
	if (world == null) {
	    executor.sendMessage(
		    String.format(ChatColor.RED + "World \"%s\" could not be found!", args[0].toLowerCase()));
	    StringBuilder builder = new StringBuilder();
	    for (World w : server.getWorlds()) {
		builder.append(ChatColor.WHITE + ", ").append(ChatColor.GRAY).append(w.getName());
	    }
	    executor.sendMessage(ChatColor.YELLOW + "Available worlds: "
		    + (builder.length() < 1 ? ChatColor.RED + "None" : builder.substring(4)));
	    return;
	}

	executor.sendMessage(ChatColor.YELLOW + "Map " + world.getName());
	executor.sendMessage(String.format(ChatColor.GRAY + "Sizes: " + ChatColor.WHITE + "(%d, %d, %d)",
		world.getWidth(), world.getHeight(), world.getLength()));
	executor.sendMessage(ChatColor.GRAY + "Total blocks: " + ChatColor.WHITE
		+ (world.getWidth() * world.getHeight() * world.getLength()));
    }
}
