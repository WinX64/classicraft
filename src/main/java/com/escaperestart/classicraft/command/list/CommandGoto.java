package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.world.World;

public class CommandGoto extends Command {

    public CommandGoto(Server server) {
	super(server, "goto", "Warps to another world", "classicraft.command.goto", "warp", "g");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	if (!(executor instanceof Player)) {
	    executor.sendMessage(ChatColor.RED + "This command cannot be used via console!");
	    return;
	}

	if (args.length < 1) {
	    executor.sendMessage(String.format(ChatColor.RED + "Syntax: /%s <world>", alias));
	    return;
	}

	World world = server.getWorld(args[0]);
	if (world == null) {
	    executor.sendMessage(
		    String.format(ChatColor.RED + "World \"%s\" could not be found!", args[0].toLowerCase()));
	    StringBuilder builder = new StringBuilder();
	    for (World w : server.getWorlds()) {
		builder.append(ChatColor.WHITE + ", ").append(ChatColor.GRAY).append(w.getName());
	    }
	    executor.sendMessage(ChatColor.YELLOW + "Available worlds: "
		    + (builder.length() < 1 ? ChatColor.RED + "None" : builder.substring(4)));
	    return;
	}

	world.join((Player) executor);
    }
}
