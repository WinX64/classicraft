package com.escaperestart.classicraft.command.list;

import com.escaperestart.classicraft.command.Command;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class CommandTPS extends Command {

    public CommandTPS(Server server) {
	super(server, "tps", "Shows the server's TPS", "classicraft.command.tps", "ticks", "lag");
    }

    @Override
    public void execute(CommandExecutor executor, String alias, String[] args) {
	executor.sendMessage(ChatColor.GOLD + "TPS: " + ChatColor.GREEN + server.getTicker().getTps());
    }
}
