package com.escaperestart.classicraft.command;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.command.list.CommandGoto;
import com.escaperestart.classicraft.command.list.CommandHacks;
import com.escaperestart.classicraft.command.list.CommandHelp;
import com.escaperestart.classicraft.command.list.CommandLoad;
import com.escaperestart.classicraft.command.list.CommandMap;
import com.escaperestart.classicraft.command.list.CommandNoClip;
import com.escaperestart.classicraft.command.list.CommandOnline;
import com.escaperestart.classicraft.command.list.CommandPermissions;
import com.escaperestart.classicraft.command.list.CommandStop;
import com.escaperestart.classicraft.command.list.CommandTPS;
import com.escaperestart.classicraft.command.list.CommandTeleport;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

public class CommandList {

    private static final Logger LOGGER = LogManager.getLogger();

    private Server server;
    private Map<String, Command> commandMap;

    public CommandList(Server server) {
	this.server = server;
	this.commandMap = new HashMap<String, Command>();
    }

    public void registerCommand(Command command) {
	commandMap.put(command.getName().toLowerCase(), command);
	for (String alias : command.getAliases()) {
	    commandMap.put(alias.toLowerCase(), command);
	}
    }

    public Map<String, Command> getCommandMap() {
	return ImmutableMap.copyOf(commandMap);
    }

    public Collection<Command> getCommands() {
	return ImmutableList.copyOf(commandMap.values());
    }

    public Command getCommand(String alias) {
	return commandMap.get(alias.toLowerCase());
    }

    public void registerDefaults() {
	registerCommand(new CommandStop(server));
	registerCommand(new CommandTPS(server));
	registerCommand(new CommandOnline(server));
	registerCommand(new CommandPermissions(server));
	registerCommand(new CommandGoto(server));
	registerCommand(new CommandTeleport(server));
	registerCommand(new CommandHacks(server));
	registerCommand(new CommandNoClip(server));
	registerCommand(new CommandLoad(server));
	registerCommand(new CommandHelp(server));
	registerCommand(new CommandMap(server));
    }

    public void handleCommand(CommandExecutor executor, String rawCommand) {
	String[] rawArgs = rawCommand.split(" ");

	Command command = commandMap.get(rawArgs[0].toLowerCase());
	String[] args = Arrays.copyOfRange(rawArgs, 1, rawArgs.length);

	if (command == null) {
	    executor.sendMessage("Unknown command! Use \"help\" to get more info on commands!");
	    return;
	}

	if (!executor.hasPermission(command.getPermission())) {
	    executor.sendMessage(ChatColor.RED + "You don't have permission to use this command!");
	    return;
	}

	try {
	    command.execute(executor, rawArgs[0].toLowerCase(), args);
	} catch (Exception e) {
	    executor.sendMessage(ChatColor.RED + "An internal error occurred while trying to execute your command!");
	    LOGGER.error("An error occurred while trying execute the command from {}. Details below:",
		    executor.getName());
	    e.printStackTrace();
	}
    }
}
