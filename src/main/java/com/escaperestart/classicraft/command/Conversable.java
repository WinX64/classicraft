package com.escaperestart.classicraft.command;

public interface Conversable {

    String getName();
    
    void sendMessage(String message);
}
