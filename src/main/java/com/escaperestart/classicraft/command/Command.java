package com.escaperestart.classicraft.command;

import com.escaperestart.classicraft.server.Server;

public abstract class Command {

    protected Server server;
    protected String name;
    protected String description;
    protected String permission;
    protected String[] aliases;

    public Command(Server server, String name, String description, String permission, String... aliases) {
	this.server = server;
	this.name = name;
	this.description = description;
	this.permission = permission;
	this.aliases = aliases;
    }

    public String getName() {
	return name;
    }

    public String getDescription() {
	return description;
    }

    public String getPermission() {
	return permission;
    }

    public String[] getAliases() {
	return aliases;
    }

    public abstract void execute(CommandExecutor executor, String alias, String[] args);
}
