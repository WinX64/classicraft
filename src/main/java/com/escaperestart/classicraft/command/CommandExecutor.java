package com.escaperestart.classicraft.command;

import com.escaperestart.classicraft.permission.PermissionHolder;

public interface CommandExecutor extends Conversable, PermissionHolder {

    void executeCommand(String rawCommand);
}
