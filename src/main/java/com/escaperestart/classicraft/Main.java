package com.escaperestart.classicraft;

import org.fusesource.jansi.AnsiConsole;

import com.escaperestart.classicraft.server.Server;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

public class Main {

	public static void main(String[] args) throws Exception {
		OptionParser parser = new OptionParser();
		parser.accepts("nojline", "Disables JLine");

		OptionSet set = parser.parse(args);
		if (!set.has("nojline")) {
			AnsiConsole.systemInstall();
		}
		System.out.println(set.has("nojline"));
		Server.getInstance().start(set);
	}
}
