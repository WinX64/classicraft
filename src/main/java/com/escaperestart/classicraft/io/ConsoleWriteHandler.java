package com.escaperestart.classicraft.io;

import static org.fusesource.jansi.Ansi.ansi;

import java.io.OutputStream;

import org.fusesource.jansi.Ansi;

import com.escaperestart.classicraft.logging.LogQueueAppender;
import com.escaperestart.classicraft.server.Server;

import jline.ConsoleReader;

public class ConsoleWriteHandler extends Thread {

    private static final byte[] CLEAR = ansi().eraseLine(Ansi.Erase.ALL).a('\r').toString().getBytes();
    private static final byte[] RESET = ansi().reset().toString().getBytes();

    private final Server server;
    private final ConsoleReader reader;
    private final OutputStream writer;

    public ConsoleWriteHandler(Server server, ConsoleReader reader, OutputStream writer) {
	super("Console Writer");
	this.server = server;
	this.reader = reader;
	this.writer = writer;

	this.setDaemon(true);
    }

    @Override
    public void run() {
	while (true) {
	    try {
		String message = LogQueueAppender.getNextMessage();
		if (message == null) {
		    continue;
		}

		if (server.getOptions().has("nojline")) {
		    writer.write(message.getBytes());
		    writer.flush();
		} else {
		    writer.write(CLEAR);
		    writer.write(message.getBytes());
		    writer.write(RESET);
		    writer.flush();
		    reader.drawLine();
		    reader.flushConsole();
		}
	    } catch (Exception e) {}
	}
    }
}
