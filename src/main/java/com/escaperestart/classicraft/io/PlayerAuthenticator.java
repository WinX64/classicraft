package com.escaperestart.classicraft.io;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.handler.LoginPacketHandler;
import com.escaperestart.classicraft.network.packet.in.PacketInIdentification;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class PlayerAuthenticator extends Thread {

    public static final AtomicInteger COUNT = new AtomicInteger();
    private static final Logger LOGGER = LogManager.getLogger();

    private Server server;
    private ClientConnection clientConnection;
    private String playerName;
    private String verificationKey;
    private LoginPacketHandler loginPacketHandler;

    public PlayerAuthenticator(Server server, ClientConnection clientConnection, PacketInIdentification packet,
	    LoginPacketHandler loginPacketHandler) {
	this.setName("Player Authenticator #" + COUNT.incrementAndGet());
	this.server = server;
	this.clientConnection = clientConnection;
	this.playerName = packet.getPlayerName();
	this.verificationKey = packet.getVerificationKey();
	this.loginPacketHandler = loginPacketHandler;
    }

    @Override
    public void run() {
	try {
	    boolean isValidLogin = isValidLogin();

	    if (!clientConnection.isConnected()) {
		return;
	    }

	    if (isValidLogin) {
		loginPacketHandler.completeAuthentication(playerName);
	    } else {
		LOGGER.warn("Player {} tried to login with an invalid verification key: {}", playerName,
			verificationKey);
		clientConnection.close("Invalid login key", ChatColor.RED + "Invalid login key!");
	    }
	} catch (Exception e) {
	    LOGGER.error("Failed to authenticate login for player {}. Details below:", playerName);
	    e.printStackTrace();
	    clientConnection.close("Failed to authenticate", ChatColor.RED + "Internal error! " + e.toString());
	} finally {
	    server.getTicker().finishAuthentication(this);
	}
    }

    private boolean isValidLogin() throws Exception {
	if (playerName.equals("WinX64")) {
	    return verificationKey.startsWith("123");
	}
	return true;
    }
}
