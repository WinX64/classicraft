package com.escaperestart.classicraft.io;

import java.io.IOException;

import com.escaperestart.classicraft.server.Server;

import jline.ConsoleReader;

public class ConsoleReadHandler extends Thread {

	private final Server server;
	private final ConsoleReader input;

	public ConsoleReadHandler(Server server, ConsoleReader input) {
		super("Console Reader");
		this.server = server;
		this.input = input;
		this.setDaemon(true);
	}

	public ConsoleReader getReader() {
		return input;
	}

	@Override
	public void run() {
		while (true) {
			try {
				String message = null;
				if (server.getOptions().has("nojline")) {
					message = input.readLine().trim();
				} else {
					message = input.readLine(">").trim();
				}
				server.getConsoleCommandExecutor().queueCommand(message);
			} catch (IOException e) {}
		}
	}
}
