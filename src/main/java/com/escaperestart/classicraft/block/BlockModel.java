package com.escaperestart.classicraft.block;

public enum BlockModel {

    OPAQUE,
    TRANSPARENT,
    TRANSPARENT_NO_CULING,
    TRANSLUCENT,
    GAS;

    public static BlockModel getBlockModel(int id) {
	return id < 0 || id >= values().length ? null : values()[id];
    }
}
