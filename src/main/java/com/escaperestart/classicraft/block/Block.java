package com.escaperestart.classicraft.block;

import java.util.HashMap;
import java.util.Map;

import com.escaperestart.classicraft.block.physics.BlockPhysics;
import com.escaperestart.classicraft.block.physics.list.DirtPhysics;
import com.escaperestart.classicraft.block.physics.list.FallingBlockPhysics;
import com.escaperestart.classicraft.block.physics.list.LavaPhysics;
import com.escaperestart.classicraft.block.physics.list.SlabPhysics;
import com.escaperestart.classicraft.block.physics.list.WaterPhysics;
import com.escaperestart.classicraft.util.AABB;

public enum Block {

    AIR(0, BlockType.AIR, null),
    STONE(1, BlockType.STONE, null),
    GRASS(2, BlockType.GRASS, null),
    DIRT(3, BlockType.DIRT, new DirtPhysics()),
    COBBLESTONE(4, BlockType.COBBLESTONE, null),
    WOOD(5, BlockType.WOOD, null),
    SAPLING(6, BlockType.SAPLING, null),
    BEDROCK(7, BlockType.BEDROCK, null),
    WATER(8, BlockType.WATER, new WaterPhysics(2)),
    STATIONARY_WATER(9, BlockType.STATIONARY_WATER, null),
    LAVA(10, BlockType.LAVA, new LavaPhysics(2)),
    STATIONARY_LAVA(11, BlockType.STATIONARY_LAVA, null),
    SAND(12, BlockType.SAND, new FallingBlockPhysics()),
    GRAVEL(13, BlockType.GRAVEL, new FallingBlockPhysics()),
    GOLD_ORE(14, BlockType.GOLD_ORE, null),
    IRON_ORE(15, BlockType.IRON_ORE, null),
    COAL_ORE(16, BlockType.COAL_ORE, null),
    LOG(17, BlockType.LOG, null),
    LEAVES(18, BlockType.LEAVES, null),
    SPONGE(19, BlockType.SPONGE, null),
    GLASS(20, BlockType.GLASS, null),
    RED_CLOTH(21, BlockType.RED_CLOTH, null),
    ORANGE_CLOTH(22, BlockType.ORANGE_CLOTH, null),
    YELLOW_CLOTH(23, BlockType.YELLOW_CLOTH, null),
    LIME_CLOTH(24, BlockType.LIME_CLOTH, null),
    GREEN_CLOTH(25, BlockType.GREEN_CLOTH, null),
    AQUA_CLOTH(26, BlockType.AQUA_CLOTH, null),
    CYAN_CLOTH(27, BlockType.CYAN_CLOTH, null),
    BLUE_CLOTH(28, BlockType.BLUE_CLOTH, null),
    PURPLE_CLOTH(29, BlockType.PURPLE_CLOTH, null),
    INDIGO_CLOTH(30, BlockType.INDIGO_CLOTH, null),
    VIOLET_CLOTH(31, BlockType.VIOLET_CLOTH, null),
    MAGENTA_CLOTH(32, BlockType.MAGENTA_CLOTH, null),
    PINK_CLOTH(33, BlockType.PINK_CLOTH, null),
    BLACK_CLOTH(34, BlockType.BLACK_CLOTH, null),
    GRAY_CLOTH(35, BlockType.GRAY_CLOTH, null),
    WHITE_CLOTH(36, BlockType.WHITE_CLOTH, null),
    YELLOW_FLOWER(37, BlockType.YELLOW_FLOWER, null),
    RED_FLOWER(38, BlockType.RED_FLOWER, null),
    BROWN_MUSHROOM(39, BlockType.BROWN_MUSHROOM, null),
    RED_MUSHROOM(40, BlockType.RED_MUSHROOM, null),
    GOLD_BLOCK(41, BlockType.GOLD_BLOCK, null),
    IRON_BLOCK(42, BlockType.IRON_BLOCK, null),
    DOUBLE_SLAB(43, BlockType.DOUBLE_SLAB, null),
    SLAB(44, BlockType.SLAB, new SlabPhysics()),
    BRICK(45, BlockType.BRICK, null),
    TNT(46, BlockType.TNT, null),
    BOOKSHELF(47, BlockType.BOOKSHELF, null),
    MOSSY_COBBLESTONE(48, BlockType.MOSSY_COBBLESTONE, null),
    OBSIDIAN(49, BlockType.OBSIDIAN, null),
    BLOCK_50(50, BlockType.AIR, null),
    BLOCK_51(51, BlockType.AIR, null),
    BLOCK_52(52, BlockType.AIR, null),
    BLOCK_53(53, BlockType.AIR, null),
    BLOCK_54(54, BlockType.AIR, null),
    BLOCK_128(128, BlockType.AIR, null),
    BLOCK_129(129, BlockType.AIR, null),
    BLOCK_130(130, BlockType.AIR, null),
    BLOCK_131(131, BlockType.AIR, null),
    BLOCK_132(132, BlockType.AIR, null);
    private static final Map<Integer, Block> BLOCKS_BY_ID = new HashMap<Integer, Block>();

    static {
	for (Block block : values()) {
	    BLOCKS_BY_ID.put((int) block.id, block);
	}
    }

    private byte id;
    private BlockType type;
    private BlockPhysics physics;

    private Block(int id, BlockType type, BlockPhysics physics) {
	this.id = (byte) id;
	this.type = type;
	this.physics = physics;
    }

    public byte getId() {
	return id;
    }

    public byte getInternalId() {
	return type.getId();
    }

    public AABB getAABB() {
	return type.getAABB();
    }

    public BlockType getType() {
	return type;
    }

    public BlockPhysics getPhysics() {
	return physics;
    }

    public boolean isWater() {
	return this == WATER || this == STATIONARY_WATER;
    }

    public boolean isLava() {
	return this == LAVA || this == STATIONARY_LAVA;
    }

    public static Block getBlock(int id) {
	return BLOCKS_BY_ID.get(id);
    }
}
