package com.escaperestart.classicraft.block;

import java.util.HashMap;
import java.util.Map;

public enum BlockFace {

    NORTH(1, 0, 0),
    SOUTH(-1, 0, 0),
    UP(0, 1, 0),
    DOWN(0, -1, 0),
    EAST(0, 0, 1),
    WEST(0, 0, -1);

    private static final Map<Integer, BlockFace> BY_ID = new HashMap<Integer, BlockFace>();

    static {
	for (BlockFace face : values()) {
	    BY_ID.put(face.ordinal() - 1, face);
	}
    }

    private byte modX;
    private byte modY;
    private byte modZ;

    private BlockFace(int modX, int modY, int modZ) {
	this.modX = (byte) modX;
	this.modY = (byte) modY;
	this.modZ = (byte) modZ;
    }

    public byte getModX() {
	return modX;
    }

    public byte getModY() {
	return modY;
    }

    public byte getModZ() {
	return modZ;
    }

    public static BlockFace getBlockFace(int id) {
	return BY_ID.get(id);
    }
}
