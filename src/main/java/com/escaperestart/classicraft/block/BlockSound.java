package com.escaperestart.classicraft.block;

public enum BlockSound {

    NO_SOUND,
    WOOD,
    GRAVEL,
    GRASS,
    STONE,
    METAL,
    GLASS,
    WOOL,
    SAND,
    SNOW;

    public static BlockSound getBlockSound(int id) {
	return id < 0 || id >= values().length ? null : values()[id];
    }
}
