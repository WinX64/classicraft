package com.escaperestart.classicraft.block;

import com.escaperestart.classicraft.util.AABB;

public enum BlockType {

    AIR(0),
    STONE(1, AABB.BLOCK),
    GRASS(2, AABB.BLOCK),
    DIRT(3, AABB.BLOCK),
    COBBLESTONE(4, AABB.BLOCK),
    WOOD(5, AABB.BLOCK),
    SAPLING(6),
    BEDROCK(7, AABB.BLOCK),
    WATER(8),
    STATIONARY_WATER(9),
    LAVA(10),
    STATIONARY_LAVA(11),
    SAND(12, AABB.BLOCK),
    GRAVEL(13, AABB.BLOCK),
    GOLD_ORE(14, AABB.BLOCK),
    IRON_ORE(15, AABB.BLOCK),
    COAL_ORE(16, AABB.BLOCK),
    LOG(17, AABB.BLOCK),
    LEAVES(18, AABB.BLOCK),
    SPONGE(19, AABB.BLOCK),
    GLASS(20, AABB.BLOCK),
    RED_CLOTH(21, AABB.BLOCK),
    ORANGE_CLOTH(22, AABB.BLOCK),
    YELLOW_CLOTH(23, AABB.BLOCK),
    LIME_CLOTH(24, AABB.BLOCK),
    GREEN_CLOTH(25, AABB.BLOCK),
    AQUA_CLOTH(26, AABB.BLOCK),
    CYAN_CLOTH(27, AABB.BLOCK),
    BLUE_CLOTH(28, AABB.BLOCK),
    PURPLE_CLOTH(29, AABB.BLOCK),
    INDIGO_CLOTH(30, AABB.BLOCK),
    VIOLET_CLOTH(31, AABB.BLOCK),
    MAGENTA_CLOTH(32, AABB.BLOCK),
    PINK_CLOTH(33, AABB.BLOCK),
    BLACK_CLOTH(34, AABB.BLOCK),
    GRAY_CLOTH(35, AABB.BLOCK),
    WHITE_CLOTH(36, AABB.BLOCK),
    YELLOW_FLOWER(37, AABB.BLOCK),
    RED_FLOWER(38, AABB.BLOCK),
    BROWN_MUSHROOM(39),
    RED_MUSHROOM(40),
    GOLD_BLOCK(41, AABB.BLOCK),
    IRON_BLOCK(42, AABB.BLOCK),
    DOUBLE_SLAB(43, AABB.BLOCK),
    SLAB(44, AABB.HALF_BLOCK),
    BRICK(45, AABB.BLOCK),
    TNT(46, AABB.BLOCK),
    BOOKSHELF(47, AABB.BLOCK),
    MOSSY_COBBLESTONE(48, AABB.BLOCK),
    OBSIDIAN(49, AABB.BLOCK);

    private final byte id;
    private final AABB boundingBox;

    private BlockType(int id) {
	this(id, null);
    }

    private BlockType(int id, AABB boundingBox) {
	this.id = (byte) id;
	this.boundingBox = boundingBox;
    }

    public byte getId() {
	return id;
    }

    public AABB getAABB() {
	return boundingBox;
    }
}
