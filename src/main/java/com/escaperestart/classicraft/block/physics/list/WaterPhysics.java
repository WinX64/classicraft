package com.escaperestart.classicraft.block.physics.list;

import java.util.Random;
import java.util.Set;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.block.BlockFace;
import com.escaperestart.classicraft.block.physics.BlockPhysics;
import com.escaperestart.classicraft.block.physics.PhysicsCheck;
import com.escaperestart.classicraft.world.World;

public class WaterPhysics extends BlockPhysics {

    private int tickTime;

    public WaterPhysics(int tickTime) {
	this.tickTime = tickTime;
    }

    @Override
    public boolean tickPhysics(World world, Random rand, PhysicsCheck check, Set<PhysicsCheck> checks) {
	int adjacent = -1;
	if (check.getTicks() % tickTime == 0 && check.getTicks() != 0) {
	    adjacent++;
	    for (BlockFace face : BlockFace.values()) {
		if (face == BlockFace.UP) {
		    continue;
		}

		int relativePosition = world.getRelativeBlockPosition(check.getBlockPosition(), face);
		if (relativePosition == -1) {
		    continue;
		}
		Block relativeBlock = world.getBlockAt(relativePosition);
		if (relativeBlock == Block.AIR || relativeBlock.isLava()) {
		    adjacent++;
		}

		if (face == BlockFace.DOWN && rand.nextInt(100) > 85) {
		    continue;
		}
		if (face != BlockFace.DOWN && rand.nextInt(100) > 40) {
		    continue;
		}

		if (relativeBlock == Block.AIR) {
		    world.queueBlockChange(relativePosition, Block.WATER);
		    checks.add(new PhysicsCheck(relativePosition, Block.WATER));
		    adjacent--;
		} else if (relativeBlock.isLava()) {
		    world.queueBlockChange(relativePosition, Block.OBSIDIAN);
		    adjacent--;
		}
	    }
	}
	return adjacent != 0;
    }
}
