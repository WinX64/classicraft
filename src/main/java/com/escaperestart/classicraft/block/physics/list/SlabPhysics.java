package com.escaperestart.classicraft.block.physics.list;

import java.util.Random;
import java.util.Set;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.block.BlockFace;
import com.escaperestart.classicraft.block.physics.BlockPhysics;
import com.escaperestart.classicraft.block.physics.PhysicsCheck;
import com.escaperestart.classicraft.world.World;

public class SlabPhysics extends BlockPhysics {

    @Override
    public boolean tickPhysics(World world, Random rand, PhysicsCheck check, Set<PhysicsCheck> checks) {
	int blockBelow = world.getRelativeBlockPosition(check.getBlockPosition(), BlockFace.DOWN);
	if (blockBelow != -1 && world.getBlockAt(blockBelow) == Block.SLAB) {
	    world.queueBlockChange(check.getBlockPosition(), Block.AIR);
	    world.queueBlockChange(blockBelow, Block.DOUBLE_SLAB);
	}
	return false;
    }
}
