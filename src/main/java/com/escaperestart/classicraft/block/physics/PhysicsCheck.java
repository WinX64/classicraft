package com.escaperestart.classicraft.block.physics;

import com.escaperestart.classicraft.block.Block;

public class PhysicsCheck {

    private final int blockPosition;
    private final Block block;
    private int ticks;

    public PhysicsCheck(int blockPosition, Block block) {
	this(blockPosition, block, 0);
    }

    public PhysicsCheck(int blockPosition, Block block, int ticks) {
	this.blockPosition = blockPosition;
	this.block = block;
	this.ticks = ticks;
    }

    public int getBlockPosition() {
	return blockPosition;
    }

    public Block getBlock() {
	return block;
    }

    public int getTicks() {
	return ticks;
    }

    public void setTicks(int ticks) {
	this.ticks = ticks;
    }

    public void tick() {
	this.ticks++;
    }

    @Override
    public int hashCode() {
	return blockPosition;
    }

    @Override
    public boolean equals(Object obj) {
	if (!(obj instanceof PhysicsCheck)) {
	    return false;
	}

	return ((PhysicsCheck) obj).blockPosition == this.blockPosition;
    }
    
    @Override
    public String toString() {
	return block.name() + "@" + blockPosition;
    }
}
