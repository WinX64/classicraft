package com.escaperestart.classicraft.block.physics.list;

import java.util.Random;
import java.util.Set;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.block.BlockFace;
import com.escaperestart.classicraft.block.physics.BlockPhysics;
import com.escaperestart.classicraft.block.physics.PhysicsCheck;
import com.escaperestart.classicraft.world.World;

public class DirtPhysics extends BlockPhysics {

    @Override
    public boolean tickPhysics(World world, Random rand, PhysicsCheck check, Set<PhysicsCheck> checks) {
	int blockBelow = world.getRelativeBlockPosition(check.getBlockPosition(), BlockFace.DOWN);
	if (blockBelow != -1 && world.getBlockAt(blockBelow) == Block.GRASS) {
	    world.queueBlockChange(blockBelow, Block.DIRT);
	}
	int blockAbove = world.getRelativeBlockPosition(check.getBlockPosition(), BlockFace.UP);
	if (blockAbove == -1 || world.getBlockAt(blockAbove) == Block.AIR) {
	    world.queueBlockChange(check.getBlockPosition(), Block.GRASS);
	}
	return false;
    }
}
