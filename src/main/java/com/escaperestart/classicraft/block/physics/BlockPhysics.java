package com.escaperestart.classicraft.block.physics;

import java.util.Random;
import java.util.Set;

import com.escaperestart.classicraft.world.World;

public abstract class BlockPhysics {

    public abstract boolean tickPhysics(World world, Random rand, PhysicsCheck check, Set<PhysicsCheck> checks);
}
