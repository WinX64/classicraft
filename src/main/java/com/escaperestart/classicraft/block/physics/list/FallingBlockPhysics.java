package com.escaperestart.classicraft.block.physics.list;

import java.util.Random;
import java.util.Set;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.block.BlockFace;
import com.escaperestart.classicraft.block.physics.BlockPhysics;
import com.escaperestart.classicraft.block.physics.PhysicsCheck;
import com.escaperestart.classicraft.world.World;

public class FallingBlockPhysics extends BlockPhysics {

    @Override
    public boolean tickPhysics(World world, Random rand, PhysicsCheck check, Set<PhysicsCheck> checks) {
	int blockBelow = check.getBlockPosition();
	while (true) {
	    int nextBlockBelow = world.getRelativeBlockPosition(blockBelow, BlockFace.DOWN);
	    if (nextBlockBelow == -1) {
		break;
	    }
	    Block below = world.getBlockAt(nextBlockBelow);
	    if (below != Block.AIR) {
		if (below.isWater() || below.isLava()) {
		    world.queueBlockChange(nextBlockBelow, Block.AIR);
		} else {
		    break;
		}
	    }
	    blockBelow = nextBlockBelow;
	}
	if (check.getBlockPosition() != blockBelow) {
	    world.queueBlockChange(check.getBlockPosition(), Block.AIR);
	    for (BlockFace face : BlockFace.values()) {
		if (face == BlockFace.DOWN) {
		    continue;
		}
		int relativeBlock = world.getRelativeBlockPosition(check.getBlockPosition(), face);
		if (relativeBlock == -1) {
		    continue;
		}
		Block relative = world.getBlockAt(relativeBlock);
		checks.add(new PhysicsCheck(relativeBlock, relative));
	    }
	    world.queueBlockChange(blockBelow, check.getBlock());
	    for (BlockFace face : BlockFace.values()) {
		if (face == BlockFace.UP) {
		    continue;
		}
		int relativeBlock = world.getRelativeBlockPosition(blockBelow, face);
		if (relativeBlock == -1) {
		    continue;
		}
		Block relative = world.getBlockAt(relativeBlock);
		checks.add(new PhysicsCheck(relativeBlock, relative));
	    }

	}
	return false;
    }
}
