package com.escaperestart.classicraft.block;

public enum BlockSolidity {

    WALK_THROUGH,
    SWIM_THROUGH,
    SOLID;

    public static BlockSolidity getBlockSolidity(int id) {
	return id < 0 || id >= values().length ? null : values()[id];
    }
}
