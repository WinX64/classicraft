package com.escaperestart.classicraft.logging;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

public class LoggingOutputStream extends ByteArrayOutputStream {

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

	private final Logger logger;
	private final Level level;

	public LoggingOutputStream(Logger logger, Level level) {
		this.logger = logger;
		this.level = level;
	}

	@Override
	public void flush() throws IOException {
		synchronized (this) {
			super.flush();
			String message = super.toString();
			super.reset();
			if (message.length() > 0 && !message.equals(LINE_SEPARATOR)) {
				logger.log(level, message);
			}
		}
	}
}
