package com.escaperestart.classicraft.logging;

import java.io.Serializable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;

@Plugin(name = "LogQueue", category = "Core", elementType = "appender", printObject = true)
public class LogQueueAppender extends AbstractAppender {

    private static final long serialVersionUID = 4752414981414313425L;
    private static final BlockingQueue<String> QUEUED_MESSAGES = new LinkedBlockingQueue<String>();

    public LogQueueAppender(String name, Filter filter, Layout<? extends Serializable> layout,
	    boolean ignoreExceptions) {
	super(name, filter, layout, ignoreExceptions);
	//System.out.println("Created! " + name + ", " + filter + ", " + layout + ", " + ignoreExceptions);
    }

    @Override
    public void append(LogEvent event) {
	//System.out.println("Appended! " + event.getMessage().getFormattedMessage() + "... " + QUEUED_MESSAGES.size());
	QUEUED_MESSAGES.offer(this.getLayout().toSerializable(event).toString());
    }

    public static String getNextMessage() {
	try {
	    return QUEUED_MESSAGES.take();
	} catch (InterruptedException e) {
	    return null;
	}
    }

    @PluginFactory
    public static LogQueueAppender createAppender(@PluginAttribute("name") String name,
	    @PluginAttribute("ignoreExceptions") String ignoreExceptions,
	    @PluginElement("Layout") Layout<? extends Serializable> layout, @PluginElement("Filters") Filter filters) {

	boolean ignore = Boolean.parseBoolean(ignoreExceptions);

	if (name == null) {
	    return null;
	}

	if (layout == null) {
	    layout = PatternLayout.createDefaultLayout();
	}

	return new LogQueueAppender(name, filters, layout, ignore);
    }
}
