package com.escaperestart.classicraft.permission;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class PermissionManager {

    private static final Logger LOGGER = LogManager.getLogger();

    private Set<Rank> ranks;
    private Map<String, Rank> ranksByAlias;
    private TreeMap<Integer, Rank> ranksByPermissionLevel;
    private Rank defaultRank;

    public PermissionManager(Server server) {
	this.ranks = new HashSet<Rank>();
	this.ranksByAlias = new LinkedHashMap<String, Rank>();
	this.ranksByPermissionLevel = new TreeMap<Integer, Rank>();
	this.defaultRank = null;
    }

    public void registerRank(String name, int permissionLevel, ChatColor color, String... aliases) {
	Rank rank = new Rank(name, permissionLevel, color, aliases);
	rank.addPermission("classicraft.command.tps");
	rank.addPermission("classicraft.command.online");
	rank.addPermission("classicraft.command.permissions");
	rank.addPermission("classicraft.command.goto");
	rank.addPermission("classicraft.command.teleport");
	rank.addPermission("classicraft.command.hacks");
	rank.addPermission("classicraft.command.noclip");
	rank.addPermission("classicraft.command.load");
	rank.addPermission("classicraft.command.help");
	rank.addPermission("classicraft.command.stop");
	rank.addPermission("classicraft.command.map");
	if (ranksByAlias.containsKey(name.toLowerCase())) {
	    LOGGER.warn("[Rank] Duplicate rank name \"{}\", ignoring rank altogether!", name);
	    return;
	}
	if (ranksByPermissionLevel.containsKey(permissionLevel)) {
	    LOGGER.warn("[Rank] Duplicate rank permission level {} for rank \"{}\", ignoring rank altogether!",
		    permissionLevel, name);
	    return;
	}
	ranks.add(rank);
	ranksByAlias.put(name.toLowerCase(), rank);
	ranksByPermissionLevel.put(permissionLevel, rank);
	for (String alias : aliases) {
	    if (alias.equalsIgnoreCase(name)) {
		LOGGER.warn("[Rank] Rank name and alias are equal for rank \"{}\", ignoring alias!", name);
		continue;
	    }
	    if (ranksByAlias.containsKey(alias.toLowerCase())) {
		LOGGER.warn("[Rank] Duplicate alias rank \"{}\", ignoring alias!", alias);
		continue;
	    }
	    ranksByAlias.put(alias.toLowerCase(), rank);
	}
	LOGGER.info("[Rank] Rank \"{}\" registered!", name);
    }

    public void registerDefaults() {
	registerRank("Admin", 0, ChatColor.DARK_RED, "superop", "director", "headcontroller", "hc");
	registerRank("Controller", 100, ChatColor.AQUA, "cont");
	registerRank("VetOp", 200, ChatColor.DARK_GREEN, "vet", "veteran", "veteranop");
	registerRank("Operator", 300, ChatColor.DARK_PURPLE, "op", "mod", "moderator");
	registerRank("Trusted", 400, ChatColor.GOLD, "trialmod", "tmod");
	registerRank("Master", 500, ChatColor.BLUE);
	registerRank("AdvBuilder", 600, ChatColor.DARK_AQUA, "adv");
	registerRank("Builder", 700, ChatColor.DARK_GRAY);
	registerRank("Crafter", 800, ChatColor.GRAY);
	registerRank("Player", 900, ChatColor.WHITE, "user", "guest");

	this.defaultRank = ranksByPermissionLevel.firstEntry().getValue();

	LOGGER.info(this.ranksByPermissionLevel.toString());
    }

    public Rank getDefaultRank() {
	return defaultRank;
    }

    public Rank getRank(String alias) {
	return ranksByAlias.get(alias.toLowerCase());
    }

    public Set<Rank> getRanks() {
	return Collections.unmodifiableSet(ranks);
    }
}
