package com.escaperestart.classicraft.permission;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.escaperestart.classicraft.util.ChatColor;

public class Rank {

    private String name;
    private int permissionLevel;
    private ChatColor color;
    private String[] aliases;

    private Set<String> permissions;

    Rank(String name, int permissionLevel, ChatColor color, String... aliases) {
	this.name = name;
	this.permissionLevel = permissionLevel;
	this.color = color;
	this.aliases = aliases;

	this.permissions = new HashSet<String>();
    }

    public String getName() {
	return name;
    }

    public int getPermissionLevel() {
	return permissionLevel;
    }

    public ChatColor getColor() {
	return color;
    }

    public String getColoredName() {
	return color + name;
    }

    public String[] getAliases() {
	return aliases;
    }

    public void addPermission(String permission) {
	this.permissions.add(permission.toLowerCase());
    }

    public void removePermission(String permission) {
	this.permissions.remove(permission.toLowerCase());
    }

    public boolean hasPermission(String permission) {
	return permissions.contains(permission.toLowerCase());
    }

    public Set<String> getPermissions() {
	return Collections.unmodifiableSet(permissions);
    }

    @Override
    public String toString() {
	return String.format("{ %s, %d, %s }", name, permissionLevel, color.name());
    }
}
