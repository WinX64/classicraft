package com.escaperestart.classicraft.permission;

public interface PermissionHolder {

    boolean hasPermission(String permission);
}
