package com.escaperestart.classicraft.world;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import com.escaperestart.classicraft.server.Server;

public class WorldCreator {

    public static final World loadWorldFromFile(Server server, File file) {
	DataInputStream input = null;
	GZIPInputStream zip = null;
	try {
	    String level = file.getName().substring(0, file.getName().indexOf('.'));
	    zip = new GZIPInputStream(new FileInputStream(file), 65536);
	    input = new DataInputStream(zip);

	    short sizeX = input.readShort();
	    short sizeY = input.readShort();
	    short sizeZ = input.readShort();

	    short spawnX = input.readShort();
	    short spawnY = input.readShort();
	    short spawnZ = input.readShort();
	    byte spawnRotX = input.readByte();
	    byte spawnRotY = input.readByte();
	    byte[] blocks = new byte[sizeX * sizeY * sizeZ];
	    input.read(blocks, 0, blocks.length);

	    World world = new World(server, level, sizeX, sizeY, sizeZ, "");
	    world.setSpawnX((short) (spawnX << 5));
	    world.setSpawnY((short) (spawnY << 5));
	    world.setSpawnZ((short) (spawnZ << 5));
	    world.setSpawnRotX(spawnRotX);
	    world.setSpawnRotY(spawnRotY);
	    world.populate(blocks);
	    return world;
	} catch (Exception e) {
	    return null;
	} finally {
	    if (input != null) {
		try {
		    input.close();
		} catch (IOException e) {}
	    }
	    if (zip != null) {
		try {
		    zip.close();
		} catch (IOException e) {}
	    }
	}
    }
}
