package com.escaperestart.classicraft.world;

import java.util.HashMap;
import java.util.Map;

public enum WeatherType {

    SUNNY,
    RAINING,
    SNOWING;

    private static final Map<Integer, WeatherType> BY_ID = new HashMap<Integer, WeatherType>();

    static {
	for (WeatherType weather : values()) {
	    BY_ID.put(weather.ordinal() - 1, weather);
	}
    }

    private final byte id;

    private WeatherType() {
	this.id = (byte) (ordinal() - 1);
    }

    public byte getId() {
	return id;
    }

    public static WeatherType getWeatherType(int id) {
	return BY_ID.get(id);
    }
}
