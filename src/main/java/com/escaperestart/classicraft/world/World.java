package com.escaperestart.classicraft.world;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.block.BlockFace;
import com.escaperestart.classicraft.block.physics.BlockPhysics;
import com.escaperestart.classicraft.block.physics.PhysicsCheck;
import com.escaperestart.classicraft.network.packet.out.PacketOutBlockChange;
import com.escaperestart.classicraft.network.packet.out.PacketOutBulkBlockChange;
import com.escaperestart.classicraft.network.packet.out.PacketOutLevelDataChunk;
import com.escaperestart.classicraft.network.packet.out.PacketOutLevelFinalize;
import com.escaperestart.classicraft.network.packet.out.PacketOutLevelInitialize;
import com.escaperestart.classicraft.network.packet.out.PacketOutMessage;
import com.escaperestart.classicraft.player.MessageSanitizer;
import com.escaperestart.classicraft.player.MessageType;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.AABB;
import com.escaperestart.classicraft.util.ChatColor;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Ints;

import io.netty.buffer.Unpooled;

public class World {

	private static final Logger LOGGER = LogManager.getLogger();

	private Server server;

	private final String name;
	private final List<Player> players;
	private final byte[] blocks;

	private final Random rand;
	private Set<PhysicsCheck> blockPhysics;
	private int currentTick;

	private Map<Integer, Byte> pendingBlockChanges;
	private byte[][] compressedBlocks;
	private short lastPartSize;

	private short width;
	private short height;
	private short length;

	private short spawnX;
	private short spawnY;
	private short spawnZ;
	private byte spawnRotX;
	private byte spawnRotY;

	private String type;

	public World(Server server, String name, short width, short height, short length, String type) {
		if (width % 16 != 0 || height % 16 != 0 || length % 16 != 0) {
			throw new IllegalArgumentException("The 3 sizes must be a multiple of 16");
		}
		if (width > 1024 || height > 1024 || length > 1024) {
			throw new IllegalArgumentException("None of the sizes must exceed 1024");
		}
		this.server = server;

		this.name = name;
		this.players = new ArrayList<Player>();
		this.blocks = new byte[width * height * length];

		this.rand = new Random();
		this.blockPhysics = new HashSet<PhysicsCheck>();
		this.currentTick = 0;

		this.pendingBlockChanges = new HashMap<Integer, Byte>();
		this.compressedBlocks = null;
		this.lastPartSize = 0;

		this.width = width;
		this.height = height;
		this.length = length;

		this.spawnX = (short) (((width / 2) << 5) + 16);
		this.spawnY = (short) (((height / 2) << 5) + 64);
		this.spawnZ = (short) (((length / 2) << 5) + 16);
		this.spawnRotX = 0;
		this.spawnRotY = 0;

		this.type = type.toLowerCase();

		populate();
	}

	public void populate(byte[] blocks) {
		if (blocks.length != this.blocks.length) {
			return;
		}

		for (int i = 0; i < blocks.length; i++) {
			if (Block.getBlock(blocks[i]) != null) {
				this.blocks[i] = blocks[i];
			} else {
				this.blocks[i] = Block.AIR.getId();
			}
		}
	}

	private void populate() {
		switch (type) {
			case "default":
			default:
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						for (int z = 0; z < length; z++) {
							blocks[getBlockPosition(x, y,
									z)] = (y < height / 2 ? Block.DIRT : y == height / 2 ? Block.GRASS : Block.AIR)
											.getId();
						}
					}
				}
				break;

			case "trench":
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						for (int z = 0; z < length; z++) {
							blocks[getBlockPosition(x, y,
									z)] = (x == 0 || x == width - 1 || y == 0 || z == 0 || z == length - 1
											? Block.BEDROCK : Block.AIR).getId();
						}
					}
				}
				break;

			case "pixel":
				for (int x = 0; x < width; x++) {
					for (int y = 0; y < height; y++) {
						for (int z = 0; z < length; z++) {
							blocks[getBlockPosition(x, y,
									z)] = (x == 0 || x == width - 1 || y == 0 || z == 0 || z == length - 1
											? Block.WHITE_CLOTH : Block.AIR).getId();
						}
					}
				}
				break;
		}
	}

	private void compressMapData() {
		long nano = System.nanoTime();

		ByteArrayOutputStream data = new ByteArrayOutputStream();
		GZIPOutputStream compressor = null;
		try {
			byte[] compressedData = null;
			compressor = new GZIPOutputStream(data);
			compressor.write(Unpooled.buffer(4).writeInt(width * height * length).array());
			compressor.write(blocks);
			compressor.finish();
			compressedData = data.toByteArray();

			int parts = (int) Math.ceil(compressedData.length / 1024.0);
			this.compressedBlocks = new byte[parts][];

			for (int part = 0; part < parts; part++) {
				this.compressedBlocks[part] = new byte[1024];
				int blocks = compressedData.length / ((part + 1) * 1024) > 0 ? 1024 : compressedData.length % 1024;
				this.lastPartSize = (short) blocks;
				System.arraycopy(compressedData, part * 1024, compressedBlocks[part], 0, blocks);
			}
		} catch (IOException e) {
			this.compressedBlocks = null;
		} finally {
			try {
				compressor.close();
			} catch (IOException e) {
			}
			try {
				data.close();
			} catch (IOException e) {
			}
		}

		nano = System.nanoTime() - nano;
		LOGGER.info("Compressed map chunk data for {} in {}ns", name, nano);
	}

	public String getName() {
		return name;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getLength() {
		return length;
	}

	public int getSpawnX() {
		return spawnX;
	}

	public void setSpawnX(short spawnX) {
		this.spawnX = spawnX;
	}

	public int getSpawnY() {
		return spawnY;
	}

	public void setSpawnY(short spawnY) {
		this.spawnY = spawnY;
	}

	public int getSpawnZ() {
		return spawnZ;
	}

	public void setSpawnZ(short spawnZ) {
		this.spawnZ = spawnZ;
	}

	public byte getSpawnRotX() {
		return spawnRotX;
	}

	public void setSpawnRotX(byte spawnRotX) {
		this.spawnRotX = spawnRotX;
	}

	public byte getSpawnRotY() {
		return spawnRotY;
	}

	public void setSpawnRotY(byte spawnRotY) {
		this.spawnRotY = spawnRotY;
	}

	public int getPendingPhysicsUpdates() {
		return blockPhysics.size();
	}

	public int getRelativeBlockPosition(int x, int y, int z, BlockFace relative) {
		return getBlockPosition(x + relative.getModX(), y + relative.getModY(), z + relative.getModZ());
	}

	public int getRelativeBlockPosition(int position, BlockFace relative) {
		short[] coords = getBlockCoordinates(position);
		int newX = coords[0] + relative.getModX();
		int newY = coords[1] + relative.getModY();
		int newZ = coords[2] + relative.getModZ();

		if (newX < 0 || newY < 0 || newZ < 0 || newX >= width || newY >= height || newZ >= length) {
			return -1;
		}
		return position + relative.getModX() + relative.getModZ() * width + relative.getModY() * width * length;
	}

	public int getBlockPosition(int x, int y, int z) {
		if (x < 0 || y < 0 || z < 0 || x >= width || y >= height || z >= length) {
			return -1;
		}

		return x + z * width + y * width * length;
	}

	public short[] getBlockCoordinates(int positionIndex) {
		short[] coords = new short[3];

		coords[1] = (short) (positionIndex / width / length);
		positionIndex -= coords[1] * width * length;
		coords[2] = (short) (positionIndex / width);
		positionIndex -= coords[2] * width;
		coords[0] = (short) positionIndex;

		return coords;
	}

	public List<Player> getPlayers() {
		return Collections.unmodifiableList(players);
	}

	public void join(Player player) {
		LOGGER.info("{} joined the world {}", player.getName(), name);
		if (player.getWorld() != null) {
			player.getWorld().quit(player);
		}
		player.setWorld(this);
		players.add(player);
		LOGGER.info("{} players in this world!", players.size());

		sendMapPacket(player);
		player.setPositionOrientation(spawnX, spawnY, spawnZ, spawnRotX, spawnRotY);

		player.sendSpawn(player, true);
		for (Player target : players) {
			if (target != player) {
				player.sendSpawn(target, false);
				target.sendSpawn(player, false);
			}
		}
	}

	public void quit(Player player) {
		player.sendDespawn(player, true);
		for (Player target : players) {
			if (target != player) {
				player.sendDespawn(target, false);
			}
		}
		players.remove(player);
	}

	public void sendMapPacket(Player player) {
		if (compressedBlocks == null) {
			compressMapData();
			if (compressedBlocks == null) {
				player.kick("An error occured while compresing the map data",
						ChatColor.RED + "An error occured while compressing map chunk data!");
				return;
			}
		}

		player.getClientConnection().sendPacket(new PacketOutLevelInitialize(), null);

		for (int part = 0, parts = compressedBlocks.length; part < parts; part++) {
			short dataSize = part + 1 == parts ? lastPartSize : 1024;
			byte percentage = (byte) (255.0 * part / parts);
			player.getClientConnection()
					.sendPacket(new PacketOutLevelDataChunk(dataSize, compressedBlocks[part], percentage), null);
		}
		player.getClientConnection().sendPacket(new PacketOutLevelFinalize(width, height, length), null);
	}

	public void broadcastMessage(String message) {
		List<String> parts = MessageSanitizer.sanitizeChatMessage(message);
		for (String part : parts) {
			PacketOutMessage packet = new PacketOutMessage(MessageType.CHAT, part);
			for (Player player : players) {
				player.getClientConnection().sendPacket(packet, null);
			}
		}
	}

	private long lastTickTime = 0;

	public void tick() {
		for (Player player : players) {
			player.getClientConnection().sendPacket(new PacketOutMessage(MessageType.UPPER_THREE,
					ChatColor.RED + "TPS: " + ChatColor.GRAY + String.format("%2.5f", server.getTicker().getTps())),
					null);
			player.getClientConnection().sendPacket(
					new PacketOutMessage(MessageType.UPPER_TWO, String
							.format(ChatColor.AQUA + "%s tick time: " + ChatColor.GRAY + "%08dns", name, lastTickTime)),
					null);
			player.getClientConnection()
					.sendPacket(new PacketOutMessage(MessageType.UPPER_ONE,
							String.format(ChatColor.GREEN + "%s block updates: " + ChatColor.GRAY + "%05d", name,
									this.blockPhysics.size())),
							null);

		}
		long nano = System.nanoTime();
		if (currentTick % 1 == 0) {
			Set<PhysicsCheck> newPhysics = new HashSet<PhysicsCheck>();
			for (PhysicsCheck check : blockPhysics) {
				if (check.getBlock().getId() != blocks[check.getBlockPosition()]) {
					continue;
				}

				BlockPhysics phys = check.getBlock().getPhysics();
				if (phys != null && phys.tickPhysics(this, rand, check, newPhysics)) {
					newPhysics.add(check);
				}
				check.tick();
			}
			this.blockPhysics = newPhysics;
		}
		if (this.pendingBlockChanges.size() > 0) {
			if (pendingBlockChanges.size() > 144) {
				int[] allPositions = Ints.toArray(pendingBlockChanges.keySet());
				byte[] allBlocks = Bytes.toArray(pendingBlockChanges.values());
				for (int i = 0; i < Math.ceil(pendingBlockChanges.size() / 256.0); i++) {
					int changes = (i + 1) * 256;
					if (changes >= allPositions.length) {
						changes = allPositions.length % 256;
					}
					int[] positions = Arrays.copyOfRange(allPositions, i * 256, (i + 1) * 256);
					byte[] blocks = Arrays.copyOfRange(allBlocks, i * 256, (i + 1) * 256);
					this.setBlocksAt(positions, blocks);
					this.broadcastBulkBlockChange(changes, positions, blocks);
				}
			} else {
				for (Entry<Integer, Byte> entry : this.pendingBlockChanges.entrySet()) {
					this.setBlockAt(entry.getKey(), entry.getValue());
					this.broadcastBlockChange(entry.getKey(), entry.getValue());
				}
			}
			this.pendingBlockChanges.clear();
		}
		if (currentTick % 200 == 0) {
			for (Player player : players) {
				player.sendPing();
				for (Player target : players) {
					if (player != target) {
						continue;
					}
					player.sendTeleport(target, false, true);
				}
			}
		}
		currentTick++;
		this.lastTickTime = System.nanoTime() - nano;
	}

	public void queueBlockChange(int pos, Block newBlock) {
		this.pendingBlockChanges.put(pos, newBlock.getId());
	}

	public void broadcastBlockChange(int pos, Block newBlock) {
		short[] coords = getBlockCoordinates(pos);
		broadcastBlockChange(coords[0], coords[1], coords[2], newBlock);
	}

	public void broadcastBlockChange(short x, short y, short z, Block newBlock) {
		broadcastBlockChange(x, y, z, newBlock.getId());
	}

	public void broadcastBlockChange(int pos, byte blockId) {
		short[] coords = getBlockCoordinates(pos);
		broadcastBlockChange(coords[0], coords[1], coords[2], blockId);
	}

	public void broadcastBlockChange(short x, short y, short z, byte blockId) {
		PacketOutBlockChange packet = new PacketOutBlockChange(x, y, z, blockId);
		for (Player player : players) {
			player.getClientConnection().sendPacket(packet, null);
		}
	}

	public void broadcastBulkBlockChange(int changes, int[] positions, byte[] blocks) {
		PacketOutBulkBlockChange packet = new PacketOutBulkBlockChange(changes, positions, blocks);
		for (Player player : players) {
			player.getClientConnection().sendPacket(packet, null);
		}
	}

	public Block getBlockAt(int position) {
		return Block.getBlock(blocks[position]);
	}

	public Block getBlockAt(short x, short y, short z) {
		return Block.getBlock(blocks[getBlockPosition(x, y, z)]);
	}

	public void setBlocksAt(int[] positions, byte[] blocks) {
		for (int i = 0; i < positions.length; i++) {
			this.blocks[positions[i]] = blocks[i];
		}
	}

	public void setBlockAt(int position, byte blockId) {
		blocks[position] = blockId;
	}

	public void setBlockAt(int position, Block block) {
		blocks[position] = block.getId();
	}

	public void setBlockAt(short x, short y, short z, Block block) {
		blocks[getBlockPosition(x, y, z)] = block.getId();
	}

	public void breakBlock(Player player, short x, short y, short z) {
		if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= length) {
			return;
		}

		Block currentBlock = Block.getBlock(blocks[getBlockPosition(x, y, z)]);

		if (!player.isOp() && Math.abs((spawnX >> 5) - x) < 5 && Math.abs((spawnZ >> 5) - z) < 5) {
			player.sendMessage(ChatColor.RED + "You cannot modify this close to the spawn point!");
			player.sendBlockChange(x, y, z, currentBlock);
			return;
		}

		int pos = getBlockPosition(x, y, z);
		blocks[pos] = Block.AIR.getId();
		compressedBlocks = null;
		for (Player target : players) {
			target.sendBlockChange(x, y, z, Block.AIR);
		}
		addPhysicsCheck(x - 1, y, z);
		addPhysicsCheck(x + 1, y, z);
		addPhysicsCheck(x, y - 1, z);
		addPhysicsCheck(x, y + 1, z);
		addPhysicsCheck(x, y, z - 1);
		addPhysicsCheck(x, y, z + 1);
	}

	public void buildBlock(Player player, short x, short y, short z, Block newBlock) {
		if (newBlock == null) {
			player.kick("Tried to place a block with an unknown ID", ChatColor.RED + "Unknown block type!");
			return;
		}

		if (x < 0 || x >= width || y < 0 || y >= height || z < 0 || z >= length) {
			return;
		}

		Block currentBlock = Block.getBlock(blocks[getBlockPosition(x, y, z)]);

		if (!player.isOp() && Math.abs((spawnX >> 5) - x) < 5 && Math.abs((spawnZ >> 5) - z) < 5) {
			player.sendMessage(ChatColor.RED + "You cannot modify this close to the spawn point!");
			player.sendBlockChange(x, y, z, currentBlock);
			return;
		}

		int pos = getBlockPosition(x, y, z);
		blocks[pos] = newBlock.getId();
		addPhysicsCheck(x, y, z);
		compressedBlocks = null;
		for (Player target : players) {
			target.sendBlockChange(x, y, z, newBlock);
		}
	}

	public boolean isValidPosition(Player player, int x, int y, int z) {
		AABB oldBox = player.getAABB().shrink(2, 2, 2);
		AABB newBox = AABB.PLAYER.move(x, y, z).shrink(2, 2, 2);

		boolean insideMap = newBox.getStartX() >= 0 && newBox.getStartY() >= 0 && newBox.getStartZ() >= 0
				&& newBox.getEndX() < (width << 5) && newBox.getEndY() >= 0 && newBox.getEndZ() < (length << 5);

		if (!insideMap) {
			return false;
		}

		boolean insideBlock = false;
		boolean lava = false;
		boolean water = false;

		for (short blockX = newBox.getBlockStartX(); blockX <= newBox.getBlockEndX(); blockX++) {
			for (short blockY = newBox.getBlockStartY(); blockY <= newBox.getBlockEndY(); blockY++) {
				for (short blockZ = newBox.getBlockStartZ(); blockZ <= newBox.getBlockEndZ(); blockZ++) {
					int blockPosition = this.getBlockPosition(blockX, blockY, blockZ);
					if (blockPosition != -1) {
						Block block = this.getBlockAt(blockPosition);
						if (block.isLava()) {
							lava = true;
						}
						if (block.isWater()) {
							water = true;
						}
						if (block.getAABB() != null) {
							AABB blockBox = block.getAABB().toBlockPosition(blockX, blockY, blockZ);
							if (blockBox.intersect(newBox) && !blockBox.intersect(oldBox)) {
								insideBlock = true;
							}
						}
					}
				}
			}
		}

		if (lava && water) {
			player.sendMessage(ChatColor.RED + "LAVA " + ChatColor.BLUE + "WATER");
		} else if (lava) {
			player.sendMessage(ChatColor.RED + "LAVA");
		} else if (water) {
			player.sendMessage(ChatColor.BLUE + "WATER");
		}

		return !insideBlock;
	}

	private void addPhysicsCheck(int x, int y, int z) {
		int pos = getBlockPosition(x, y, z);
		if (pos == -1) {
			return;
		}
		Block block = Block.getBlock(blocks[pos]);
		blockPhysics.add(new PhysicsCheck(pos, block));
	}
}
