package com.escaperestart.classicraft.player;

import java.util.List;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.in.PacketInPositionOrientation;
import com.escaperestart.classicraft.network.packet.out.PacketOutBlockChange;
import com.escaperestart.classicraft.network.packet.out.PacketOutBulkBlockChange;
import com.escaperestart.classicraft.network.packet.out.PacketOutDespawnPlayer;
import com.escaperestart.classicraft.network.packet.out.PacketOutMessage;
import com.escaperestart.classicraft.network.packet.out.PacketOutOrientation;
import com.escaperestart.classicraft.network.packet.out.PacketOutPing;
import com.escaperestart.classicraft.network.packet.out.PacketOutPosition;
import com.escaperestart.classicraft.network.packet.out.PacketOutPositionOrientation;
import com.escaperestart.classicraft.network.packet.out.PacketOutSpawnPlayer;
import com.escaperestart.classicraft.network.packet.out.PacketOutTeleport;
import com.escaperestart.classicraft.permission.Rank;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.AABB;
import com.escaperestart.classicraft.world.World;

public class Player implements CommandExecutor {

    public static final short TELEPORT_HEIGHT_OFFSET = 22;

    public static final byte SELF = -1;

    private Server server;
    private String name;
    private byte id;
    private UserType userType;
    private String clientName;

    private World world;
    private Rank rank;
    private AABB boundingBox;

    private boolean hacksAllowed;
    private boolean noClipAllowed;

    private short posX;
    private short posY;
    private short posZ;
    private byte yaw;
    private byte pitch;

    private short lastPosX;
    private short lastPosY;
    private short lastPosZ;
    private byte lastYaw;
    private byte lastPitch;

    private short lastTelX;
    private short lastTelY;
    private short lastTelZ;

    private ClientConnection clientConnection;

    public Player(Server server, String name, ClientConnection clientConnection) {
	this.server = server;
	this.name = name;
	this.id = -1;
	this.userType = UserType.NORMAL;
	this.clientName = "Vanilla";

	this.world = null;
	this.rank = server.getPermissionManager().getDefaultRank();
	this.boundingBox = AABB.PLAYER;

	this.hacksAllowed = true;
	this.noClipAllowed = false;

	this.posX = 0;
	this.posY = 0;
	this.posZ = 0;
	this.yaw = 0;
	this.pitch = 0;

	this.lastPosX = 0;
	this.lastPosY = 0;
	this.lastPosZ = 0;
	this.lastYaw = 0;
	this.lastPitch = 0;

	this.lastTelX = 0;
	this.lastTelY = 0;
	this.lastTelZ = 0;

	this.clientConnection = clientConnection;
	this.clientConnection.registerPlayer(this);
    }

    @Override
    public String getName() {
	return name;
    }

    public String getColoredName() {
	return rank.getColor() + name;
    }

    public byte getId() {
	return id;
    }

    public UserType getUserType() {
	return userType;
    }

    public boolean isOp() {
	return userType == UserType.OP;
    }

    public void setUserType(UserType userType) {
	this.userType = userType;
    }

    public void setOp(boolean op) {
	this.userType = op ? UserType.OP : UserType.NORMAL;
    }

    public void setClientName(String clientName) {
	this.clientName = clientName;
    }

    public String getClientName() {
	return clientName;
    }

    public void setId(byte id) {
	this.id = id;
    }

    public Rank getRank() {
	return rank;
    }

    public void setRank(Rank rank) {
	this.rank = rank;
	if (world != null) {

	}
    }

    public World getWorld() {
	return world;
    }

    public void setWorld(World world) {
	this.world = world;
    }

    public short getPosX() {
	return posX;
    }

    public void setPosX(short posX) {
	this.lastPosX = this.posX;
	this.posX = posX;
	this.boundingBox = AABB.PLAYER.move(posX, posY, posZ);
    }

    public short getPosY() {
	return posY;
    }

    public void setPosY(short posY) {
	this.lastPosY = this.posY;
	this.posY = posY;
	this.boundingBox = AABB.PLAYER.move(posX, posY, posZ);
    }

    public short getPosZ() {
	return posZ;
    }

    public void setPosZ(short posZ) {
	this.lastPosZ = this.posZ;
	this.posZ = posZ;
	this.boundingBox = AABB.PLAYER.move(posX, posY, posZ);
    }

    public byte getYaw() {
	return yaw;
    }

    public void setYaw(byte yaw) {
	this.lastYaw = this.yaw;
	this.yaw = yaw;
    }

    public byte getPitch() {
	return pitch;
    }

    public void setPitch(byte pitch) {
	this.lastPitch = this.pitch;
	this.pitch = pitch;
    }

    public void setPosition(short posX, short posY, short posZ) {
	setPositionOrientation(posX, posY, posZ, yaw, pitch);
    }

    public void setOrientation(byte yaw, byte pitch) {
	setPositionOrientation(posX, posY, posZ, yaw, pitch);
    }

    public void setPositionOrientation(short posX, short posY, short posZ, byte yaw, byte pitch) {
	this.lastPosX = this.posX;
	this.lastPosY = this.posY;
	this.lastPosZ = this.posZ;
	this.lastYaw = this.yaw;
	this.lastPitch = this.pitch;

	this.posX = posX;
	this.posY = posY;
	this.posZ = posZ;
	this.yaw = yaw;
	this.pitch = pitch;

	this.boundingBox = AABB.PLAYER.move(posX, posY, posZ);
    }

    public AABB getAABB() {
	return boundingBox;
    }

    public boolean hacksAllowed() {
	return hacksAllowed;
    }

    public void setHacksAllowed(boolean hacksAllowed) {
	this.hacksAllowed = hacksAllowed;
    }

    public boolean noClipAllowed() {
	return noClipAllowed;
    }

    public void setNoClipAllowed(boolean noClipAllowed) {
	this.noClipAllowed = noClipAllowed;
    }

    @Override
    public void sendMessage(String message) {
	List<String> parts = MessageSanitizer.sanitizeChatMessage(message);
	sendRawMessage(parts);
    }

    public void sendRawMessage(List<String> parts) {
	for (String part : parts) {
	    clientConnection.sendPacket(new PacketOutMessage(MessageType.CHAT, part), null);
	}
    }

    public void teleport(Player target) {
	if (this.world != target.world) {
	    target.world.join(this);
	}

	this.posX = target.posX;
	this.posY = target.posY;
	this.posZ = target.posZ;
	this.yaw = target.yaw;
	this.pitch = target.pitch;
	this.lastPosX = posX;
	this.lastPosY = posY;
	this.lastPosZ = posZ;
	this.lastYaw = yaw;
	this.lastPitch = pitch;

	this.sendTeleport(this, true, true);
    }

    public boolean matchesLastTeleport(PacketInPositionOrientation packet) {
	return packet.getPosX() == this.lastTelX && packet.getPosY() == this.lastTelY
		&& packet.getPosZ() == this.lastTelZ;
    }

    public void sendTeleport(Player target, boolean self, boolean forceTeleport) {
	short diffX = (short) (this.posX - this.lastPosX);
	short diffY = (short) (this.posY - this.lastPosY);
	short diffZ = (short) (this.posZ - this.lastPosZ);
	int diffYaw = this.yaw - this.lastYaw;
	int diffPitch = this.pitch - this.lastPitch;

	boolean changedPos = diffX != 0 || diffY != 0 || diffZ != 0;
	boolean changedOri = diffYaw != 0 || diffPitch != 0;
	boolean teleport = forceTeleport || Math.abs(diffX) > 32 || Math.abs(diffY) > 32 || Math.abs(diffZ) > 32;

	if (self) {
	    this.lastTelX = this.posX;
	    this.lastTelY = (short) (this.posY - TELEPORT_HEIGHT_OFFSET);
	    this.lastTelZ = this.posZ;
	}

	if (teleport || self) {
	    short fixedPosY = (short) (posY - (self ? TELEPORT_HEIGHT_OFFSET : 0));
	    target.clientConnection
		    .sendPacket(new PacketOutTeleport(self ? SELF : id, posX, fixedPosY, posZ, yaw, pitch), null);
	} else if (changedPos && changedOri) {
	    target.clientConnection.sendPacket(
		    new PacketOutPositionOrientation(id, (byte) diffX, (byte) diffY, (byte) diffZ, yaw, pitch), null);
	} else if (changedPos) {
	    target.clientConnection.sendPacket(new PacketOutPosition(id, (byte) diffX, (byte) diffY, (byte) diffZ),
		    null);
	} else if (changedOri) {
	    target.clientConnection.sendPacket(new PacketOutOrientation(id, yaw, pitch), null);
	}
    }

    public void sendSpawn(Player target, boolean self) {
	target.getClientConnection().sendPacket(
		new PacketOutSpawnPlayer(self ? Player.SELF : id, name, posX, posY, posZ, yaw, pitch), null);
    }

    public void sendBlockChange(short blockX, short blockY, short blockZ, Block newBlock) {
	clientConnection.sendPacket(new PacketOutBlockChange(blockX, blockY, blockZ, newBlock), null);
    }

    public void sendBlockChanges(int changes, int[] positions, byte[] blocks) {
	clientConnection.sendPacket(new PacketOutBulkBlockChange(changes, positions, blocks), null);
    }

    public void sendDespawn(Player target, boolean self) {
	target.clientConnection.sendPacket(new PacketOutDespawnPlayer(self ? Player.SELF : id), null);
    }

    public void sendPing() {
	clientConnection.sendPacket(new PacketOutPing(), null);
    }

    public void kick(String reason) {
	kick(reason, null);
    }

    public void kick(String reason, String disconnectionMessage) {
	clientConnection.close(reason, disconnectionMessage);
    }

    public ClientConnection getClientConnection() {
	return clientConnection;
    }

    @Override
    public void executeCommand(String rawCommand) {
	server.getCommandList().handleCommand(this, rawCommand);
    }

    @Override
    public boolean hasPermission(String permission) {
	return rank.hasPermission(permission);
    }
}
