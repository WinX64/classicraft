package com.escaperestart.classicraft.player;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.escaperestart.classicraft.util.ChatColor;

public class MessageSanitizer {

	private static final Pattern DUPLICATES = Pattern.compile("(&[0-9a-f]\\s*)+(&[0-9a-f])");
	private static final Pattern INVALID_COLOR = Pattern.compile("&[^0-9a-f]");
	private static final Pattern TRIM = Pattern.compile("[ ]+");

	public static final int MESSAGE_MAX_LENGTH = 64;

	public static List<String> sanitizeChatMessage(String message) {
		List<String> messages = new ArrayList<String>();
		boolean lineBreak = false;
		ChatColor lastColor = null;

		message = INVALID_COLOR.matcher(message).replaceAll("");
		message = DUPLICATES.matcher(message).replaceAll("$2");
		message = TRIM.matcher(message).replaceAll(" ");

		StringBuilder builder = new StringBuilder(MESSAGE_MAX_LENGTH);
		char[] messageArray = message.toCharArray();

		for (int i = 0; i < messageArray.length; i++) {
			char c = messageArray[i];
			if (c == ChatColor.COLOR_CHAR) {
				if (builder.length() == 4 && builder.charAt(2) == ChatColor.COLOR_CHAR) {
					builder.delete(2, 4);
				}
				lastColor = ChatColor.getColor(messageArray[i + 1]);
				if (i == messageArray.length - 2) {
					i++;
					lineBreak = true;
				} else {
					if (messageArray[i + 2] == ' ') {
						if (i == messageArray.length - 3) {
							i += 2;
							lineBreak = true;
						} else {
							if (builder.length() + 4 > MESSAGE_MAX_LENGTH) {
								lineBreak = true;
								i += 2;
							} else {
								builder.append(c);
							}
						}
					} else {
						if (builder.length() + 3 > MESSAGE_MAX_LENGTH) {
							lineBreak = true;
							i++;
						} else {
							builder.append(c);
						}
					}
				}
			} else {
				if (i == messageArray.length - 1 || c == '\n') {
					lineBreak = true;
				}
				if (c != '\n') {
					builder.append(c);
				}
			}
			if (builder.length() == MESSAGE_MAX_LENGTH || lineBreak) {
				lineBreak = false;
				messages.add(builder.toString());
				builder = new StringBuilder(MESSAGE_MAX_LENGTH);
				if (c != '\n') {
					builder.append("> ");
				}
				if (lastColor != null && lastColor != ChatColor.WHITE) {
					builder.append(lastColor);
				}
			}
		}
		return messages;
	}
}
