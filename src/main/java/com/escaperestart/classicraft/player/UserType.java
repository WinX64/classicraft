package com.escaperestart.classicraft.player;

public enum UserType {

    NORMAL(0),
    OP(100);

    private byte id;

    private UserType(int level) {
	this.id = (byte) level;
    }

    public byte getId() {
	return id;
    }
}
