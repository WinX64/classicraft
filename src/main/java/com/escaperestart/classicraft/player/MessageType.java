package com.escaperestart.classicraft.player;

public enum MessageType {

    CHAT(0),
    UPPER_ONE(1),
    UPPER_TWO(2),
    UPPER_THREE(3),
    LOWER_ONE(11),
    LOWER_TWO(12),
    LOWER_THREE(13),
    ANNOUNCEMENT(100);

    private final byte id;

    private MessageType(int id) {
	this.id = (byte) id;
    }

    public byte getId() {
	return id;
    }
}
