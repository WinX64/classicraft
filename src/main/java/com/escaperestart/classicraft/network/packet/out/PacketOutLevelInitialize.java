package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutLevelInitialize extends OutgoingPacket {

    public PacketOutLevelInitialize() {
	super(PacketType.PacketTypeOut.LEVEL_INITIALIZE);
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {}
}
