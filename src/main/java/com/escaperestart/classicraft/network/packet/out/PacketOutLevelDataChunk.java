package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutLevelDataChunk extends OutgoingPacket {

    private short dataSize;
    private byte[] data;
    private byte percentage;

    public PacketOutLevelDataChunk(short dataSize, byte[] data, byte percentage) {
	super(PacketType.PacketTypeOut.LEVEL_DATA_CHUNK);
	this.dataSize = dataSize;
	this.data = data;
	this.percentage = percentage;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeShort(dataSize);
	buf.writeBytes(data);
	buf.writeByte(percentage);
    }
}
