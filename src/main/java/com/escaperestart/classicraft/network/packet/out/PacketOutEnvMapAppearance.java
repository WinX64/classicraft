package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.block.BlockType;
import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;

import io.netty.buffer.ByteBuf;

public class PacketOutEnvMapAppearance extends OutgoingPacket {

    private final String textureUrl;
    private final BlockType sideBlock;
    private final BlockType edgeBlock;
    private final int sideLevel;
    private final int cloudLevel;
    private final int maxViewDistance;

    public PacketOutEnvMapAppearance(String textureUrl, BlockType sideBlock, BlockType edgeBlock, int sideLevel,
	    int cloudLevel, int maxViewDistance) {
	super(PacketType.PacketTypeOut.ENV_MAP_APPEARANCE);
	this.textureUrl = textureUrl;
	this.sideBlock = sideBlock;
	this.edgeBlock = edgeBlock;
	this.sideLevel = sideLevel;
	this.cloudLevel = cloudLevel;
	this.maxViewDistance = maxViewDistance;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeBytes(PacketEncoder.encodeString(textureUrl));
	buf.writeByte(sideBlock.getId());
	buf.writeByte(edgeBlock.getId());
	buf.writeShort(sideLevel);
	buf.writeShort(cloudLevel);
	buf.writeShort(maxViewDistance);
    }
}
