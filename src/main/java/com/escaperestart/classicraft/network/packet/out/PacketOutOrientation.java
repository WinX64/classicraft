package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutOrientation extends OutgoingPacket {

    private byte playerId;
    private short yaw;
    private short pitch;

    public PacketOutOrientation(byte playerId, short yaw, short pitch) {
	super(PacketType.PacketTypeOut.ORIENTATION);
	this.playerId = playerId;
	this.yaw = yaw;
	this.pitch = pitch;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(playerId);
	buf.writeByte(yaw);
	buf.writeByte(pitch);
    }
}
