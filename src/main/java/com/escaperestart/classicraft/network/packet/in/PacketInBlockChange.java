package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInBlockChange extends IncomingPacket {

    private short blockX;
    private short blockY;
    private short blockZ;
    private byte action;
    private Block block;

    public PacketInBlockChange() {
	super(PacketType.PacketTypeIn.BLOCK_CHANGE);
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.blockX = buf.readShort();
	this.blockY = buf.readShort();
	this.blockZ = buf.readShort();
	this.action = buf.readByte();
	this.block = Block.getBlock(buf.readByte());
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processBlockChange(this);
    }

    public short getBlockX() {
	return blockX;
    }

    public short getBlockY() {
	return blockY;
    }

    public short getBlockZ() {
	return blockZ;
    }

    public byte getAction() {
	return action;
    }

    public Block getNewBlock() {
	return block;
    }
}
