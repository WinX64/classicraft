package com.escaperestart.classicraft.network.packet.io;

import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.in.PacketInBlockChange;
import com.escaperestart.classicraft.network.packet.in.PacketInCustomBlocks;
import com.escaperestart.classicraft.network.packet.in.PacketInExtEntry;
import com.escaperestart.classicraft.network.packet.in.PacketInExtInfo;
import com.escaperestart.classicraft.network.packet.in.PacketInIdentification;
import com.escaperestart.classicraft.network.packet.in.PacketInMessage;
import com.escaperestart.classicraft.network.packet.in.PacketInPositionOrientation;
import com.escaperestart.classicraft.server.Server;

public abstract class PacketHandler {

    protected final Server server;
    protected final ClientConnection clientConnection;

    protected int currentTick;

    public PacketHandler(Server server, ClientConnection clientConnection) {
	this.server = server;
	this.clientConnection = clientConnection;

	this.currentTick = 0;
    }

    public abstract void disconnect(String reason);

    public abstract void tick();

    public abstract void processIdentification(PacketInIdentification packet);

    public abstract void processBlockChange(PacketInBlockChange packet);

    public abstract void processMessage(PacketInMessage packet);

    public abstract void processPositionOrientation(PacketInPositionOrientation packet);
    
    public abstract void processExtInfo(PacketInExtInfo packet);
    
    public abstract void processExtEntry(PacketInExtEntry packet);
    
    public abstract void processCustomBlocks(PacketInCustomBlocks packet);
}
