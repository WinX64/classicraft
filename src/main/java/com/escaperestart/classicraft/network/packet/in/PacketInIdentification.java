package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketDecoder;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInIdentification extends IncomingPacket {

    private byte protocolVersion;
    private String playerName;
    private String verificationKey;
    private byte magicalKey;

    public PacketInIdentification() {
	super(PacketType.PacketTypeIn.IDENTIFICATION);
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.protocolVersion = buf.readByte();
	this.playerName = PacketDecoder.decodeString(buf);
	this.verificationKey = PacketDecoder.decodeString(buf);
	this.magicalKey = buf.readByte();
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processIdentification(this);
    }

    public byte getProtocolVersion() {
	return protocolVersion;
    }

    public String getPlayerName() {
	return playerName;
    }

    public String getVerificationKey() {
	return verificationKey;
    }

    public byte getMagicalKey() {
	return magicalKey;
    }
}
