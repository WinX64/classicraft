package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;
import com.escaperestart.classicraft.player.MessageType;

import io.netty.buffer.ByteBuf;

public class PacketOutMessage extends OutgoingPacket {

    private final MessageType type;
    private final String message;

    public PacketOutMessage(MessageType type, String reason) {
	super(PacketType.PacketTypeOut.MESSAGE);
	this.type = type;
	this.message = reason;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(type.getId());
	buf.writeBytes(PacketEncoder.encodeString(message));
    }
}
