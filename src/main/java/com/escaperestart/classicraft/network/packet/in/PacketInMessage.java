package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketDecoder;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInMessage extends IncomingPacket {

    private byte unused;
    private String message;

    public PacketInMessage() {
	super(PacketType.PacketTypeIn.MESSAGE);
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.unused = buf.readByte();
	this.message = PacketDecoder.decodeString(buf);
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processMessage(this);
    }

    public byte getUnusedValue() {
	return unused;
    }

    public String getMessage() {
	return message;
    }
}
