package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutRemoveBlockDefinition extends OutgoingPacket {

    private final byte blockId;

    public PacketOutRemoveBlockDefinition(int blockId) {
	super(PacketType.PacketTypeOut.REMOVE_BLOCK_DEFINITION);
	this.blockId = (byte) blockId;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(blockId);
    }
}
