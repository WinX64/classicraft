package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;
import com.escaperestart.classicraft.util.ChatColor;

import io.netty.buffer.ByteBuf;

public class PacketOutDisconnect extends OutgoingPacket {

    public static final String DEFAULT_MESSAGE = ChatColor.RED + "You have been kicked from the server!";

    private String reason;

    public PacketOutDisconnect(String reason) {
	super(PacketType.PacketTypeOut.DISCONNECT);
	this.reason = reason == null ? DEFAULT_MESSAGE : reason;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeBytes(PacketEncoder.encodeString(reason));
    }
}
