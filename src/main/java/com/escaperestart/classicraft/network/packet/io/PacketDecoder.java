package com.escaperestart.classicraft.network.packet.io;

import java.util.List;

import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.Packet;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.server.Server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import io.netty.handler.codec.CorruptedFrameException;

public class PacketDecoder extends ByteToMessageDecoder {

    private ClientConnection clientConnection;

    public PacketDecoder(Server server, ClientConnection clientConnection) {
	this.clientConnection = clientConnection;
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Object> objs) throws Exception {
	if (!clientConnection.isConnected()) {
	    return;
	}

	while (buf.readableBytes() > 0) {
	    buf.markReaderIndex();

	    byte packetId = buf.readByte();
	    PacketType.PacketTypeIn packetType = PacketType.PacketTypeIn.getIncomingPacketType(packetId);

	    if (packetType == null) {
		throw new CorruptedFrameException("Unknown Packet ID: " + packetId);
	    }

	    if (buf.readableBytes() < packetType.getSize()) {
		buf.resetReaderIndex();
		break;
	    }

	    IncomingPacket packet = packetType.instantiatePacket();
	    if (packet == null) {
		throw new CorruptedFrameException("Failed to instantiate incoming packet!");
	    }
	    packet.deserializeData(buf);

	    objs.add(packet);
	}
    }

    public static final String decodeString(ByteBuf buf) throws Exception {
	byte[] encodedString = new byte[Packet.STRING_SIZE];
	buf.readBytes(encodedString);
	return new String(encodedString).trim();
    }
}
