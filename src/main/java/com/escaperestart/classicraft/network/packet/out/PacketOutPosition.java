package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutPosition extends OutgoingPacket {

    private final byte playerId;
    private final byte posX;
    private final byte posY;
    private final byte posZ;

    public PacketOutPosition(byte playerId, byte posX, byte posY, byte posZ) {
	super(PacketType.PacketTypeOut.POSTION);
	this.playerId = playerId;
	this.posX = posX;
	this.posY = posY;
	this.posZ = posZ;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(playerId);
	buf.writeByte(posX);
	buf.writeByte(posY);
	buf.writeByte(posZ);
    }
}
