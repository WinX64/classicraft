package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutCustomBlocks extends OutgoingPacket {

    private final byte supportLevel;

    public PacketOutCustomBlocks(int supportLevel) {
	super(PacketType.PacketTypeOut.CUSTOM_BLOCKS);
	this.supportLevel = (byte) supportLevel;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(supportLevel);
    }
}
