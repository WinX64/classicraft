package com.escaperestart.classicraft.network.packet;

import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public abstract class IncomingPacket extends Packet {

    public IncomingPacket(PacketType.PacketTypeIn type) {
	super(type);
    }
    
    public abstract void deserializeData(ByteBuf buf) throws Exception;

    public abstract void processPacket(PacketHandler handler) throws Exception;
}
