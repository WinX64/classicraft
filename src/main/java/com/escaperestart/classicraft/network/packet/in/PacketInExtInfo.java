package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketDecoder;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInExtInfo extends IncomingPacket {

    private String appName;
    private short extensions;

    public PacketInExtInfo() {
	super(PacketType.PacketTypeIn.EXT_INFO);
    }

    public String getAppName() {
	return appName;
    }

    public short getExtensions() {
	return extensions;
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.appName = PacketDecoder.decodeString(buf);
	this.extensions = buf.readShort();
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processExtInfo(this);
    }
}
