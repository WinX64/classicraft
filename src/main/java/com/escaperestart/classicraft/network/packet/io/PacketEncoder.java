package com.escaperestart.classicraft.network.packet.io;

import java.util.Arrays;

import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.Packet;
import com.escaperestart.classicraft.server.Server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class PacketEncoder extends MessageToByteEncoder<OutgoingPacket> {

    private ClientConnection clientConnection;

    public PacketEncoder(Server server, ClientConnection clientConnection) {
	this.clientConnection = clientConnection;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, OutgoingPacket packet, ByteBuf buf) throws Exception {
	if (!clientConnection.isConnected()) {
	    return;
	}

	buf.writeByte(packet.getPacketType().getId());
	packet.serializeData(buf);
    }

    public static final byte[] encodeString(String string) throws Exception {
	byte[] encodedString = new byte[Packet.STRING_SIZE];
	Arrays.fill(encodedString, Packet.STRING_DELIMETER);
	for (int i = 0; i < string.length(); i++) {
	    encodedString[i] = (byte) string.charAt(i);
	}
	return encodedString;
    }
}
