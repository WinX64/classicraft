package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutDespawnPlayer extends OutgoingPacket {

    private byte playerId;

    public PacketOutDespawnPlayer(byte playerId) {
	super(PacketType.PacketTypeOut.DESPAWN_PLAYER);
	this.playerId = playerId;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(playerId);
    }
}
