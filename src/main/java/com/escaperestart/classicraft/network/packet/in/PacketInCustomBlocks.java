package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInCustomBlocks extends IncomingPacket {

    private byte supportLevel;

    public PacketInCustomBlocks() {
	super(PacketType.PacketTypeIn.CUSTOM_BLOCKS);
    }
    
    public byte getSupportLevel() {
	return supportLevel;
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.supportLevel = buf.readByte();
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processCustomBlocks(this);
    }
}
