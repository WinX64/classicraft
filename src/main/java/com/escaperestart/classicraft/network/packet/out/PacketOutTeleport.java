package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutTeleport extends OutgoingPacket {

    private final byte playerId;
    private final short posX;
    private final short posY;
    private final short posZ;
    private final byte yaw;
    private final byte pitch;

    public PacketOutTeleport(byte playerId, short posX, short posY, short posZ, byte yaw, byte pitch) {
	super(PacketType.PacketTypeOut.TELEPORT);
	this.playerId = playerId;
	this.posX = posX;
	this.posY = posY;
	this.posZ = posZ;
	this.yaw = yaw;
	this.pitch = pitch;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(playerId);
	buf.writeShort(posX);
	buf.writeShort(posY);
	buf.writeShort(posZ);
	buf.writeByte(yaw);
	buf.writeByte(pitch);
    }
}
