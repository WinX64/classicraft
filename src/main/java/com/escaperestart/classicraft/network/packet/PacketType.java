package com.escaperestart.classicraft.network.packet;

import java.util.HashMap;
import java.util.Map;

import com.escaperestart.classicraft.network.packet.in.PacketInBlockChange;
import com.escaperestart.classicraft.network.packet.in.PacketInCustomBlocks;
import com.escaperestart.classicraft.network.packet.in.PacketInExtEntry;
import com.escaperestart.classicraft.network.packet.in.PacketInExtInfo;
import com.escaperestart.classicraft.network.packet.in.PacketInIdentification;
import com.escaperestart.classicraft.network.packet.in.PacketInMessage;
import com.escaperestart.classicraft.network.packet.in.PacketInPositionOrientation;
import com.escaperestart.classicraft.network.packet.out.PacketOutBlockChange;
import com.escaperestart.classicraft.network.packet.out.PacketOutBlockDefinition;
import com.escaperestart.classicraft.network.packet.out.PacketOutBlockPermissions;
import com.escaperestart.classicraft.network.packet.out.PacketOutBulkBlockChange;
import com.escaperestart.classicraft.network.packet.out.PacketOutCustomBlocks;
import com.escaperestart.classicraft.network.packet.out.PacketOutDespawnPlayer;
import com.escaperestart.classicraft.network.packet.out.PacketOutDisconnect;
import com.escaperestart.classicraft.network.packet.out.PacketOutEnvMapAppearance;
import com.escaperestart.classicraft.network.packet.out.PacketOutExtEntry;
import com.escaperestart.classicraft.network.packet.out.PacketOutExtInfo;
import com.escaperestart.classicraft.network.packet.out.PacketOutHackControl;
import com.escaperestart.classicraft.network.packet.out.PacketOutIdentification;
import com.escaperestart.classicraft.network.packet.out.PacketOutLevelDataChunk;
import com.escaperestart.classicraft.network.packet.out.PacketOutLevelFinalize;
import com.escaperestart.classicraft.network.packet.out.PacketOutLevelInitialize;
import com.escaperestart.classicraft.network.packet.out.PacketOutMessage;
import com.escaperestart.classicraft.network.packet.out.PacketOutOrientation;
import com.escaperestart.classicraft.network.packet.out.PacketOutPing;
import com.escaperestart.classicraft.network.packet.out.PacketOutPosition;
import com.escaperestart.classicraft.network.packet.out.PacketOutPositionOrientation;
import com.escaperestart.classicraft.network.packet.out.PacketOutRemoveBlockDefinition;
import com.escaperestart.classicraft.network.packet.out.PacketOutSpawnPlayer;
import com.escaperestart.classicraft.network.packet.out.PacketOutTeleport;

public interface PacketType {

    public static enum PacketTypeIn implements PacketType {
	IDENTIFICATION(0x0, 130, PacketInIdentification.class),
	BLOCK_CHANGE(0x5, 8, PacketInBlockChange.class),
	POSITION_ORIENTATION(0x8, 9, PacketInPositionOrientation.class),
	MESSAGE(0xD, 65, PacketInMessage.class),

	EXT_INFO(0x10, 66, PacketInExtInfo.class),
	EXT_ENTRY(0x11, 68, PacketInExtEntry.class),
	CUSTOM_BLOCKS(0x13, 1, PacketInCustomBlocks.class);

	private static final Map<Integer, PacketTypeIn> BY_ID = new HashMap<Integer, PacketTypeIn>();

	static {
	    for (PacketTypeIn packetIn : values()) {
		BY_ID.put((int) packetIn.id, packetIn);
	    }
	}

	private byte id;
	private int size;
	private Class<? extends IncomingPacket> referenceClass;

	private PacketTypeIn(int id, int size, Class<? extends IncomingPacket> referenceClass) {
	    this.id = (byte) id;
	    this.size = size;
	    this.referenceClass = referenceClass;
	}

	@Override
	public byte getId() {
	    return id;
	}

	@Override
	public int getSize() {
	    return size;
	}

	@Override
	public Class<? extends IncomingPacket> getDefiningClass() {
	    return referenceClass;
	}

	public IncomingPacket instantiatePacket() {
	    try {
		return this.referenceClass.newInstance();
	    } catch (InstantiationException | IllegalAccessException e) {
		e.printStackTrace();
	    }
	    return null;
	}

	public static PacketTypeIn getIncomingPacketType(int id) {
	    return BY_ID.get(id);
	}
    }

    public static enum PacketTypeOut implements PacketType {
	IDENTIFICATION(0x0, 130, PacketOutIdentification.class),
	PING(0x1, 0, PacketOutPing.class),
	LEVEL_INITIALIZE(0x2, 0, PacketOutLevelInitialize.class),
	LEVEL_DATA_CHUNK(0x3, 1027, PacketOutLevelDataChunk.class),
	LEVEL_FINALIZE(0x4, 0, PacketOutLevelFinalize.class),
	BLOCK_CHANGE(0x6, 7, PacketOutBlockChange.class),
	SPAWN_PLAYER(0x7, 73, PacketOutSpawnPlayer.class),
	TELEPORT(0x8, 9, PacketOutTeleport.class),
	POSTION_ORIENTATION(0x9, 6, PacketOutPositionOrientation.class),
	POSTION(0xA, 4, PacketOutPosition.class),
	ORIENTATION(0xB, 3, PacketOutOrientation.class),
	DESPAWN_PLAYER(0xC, 1, PacketOutDespawnPlayer.class),
	MESSAGE(0xD, 65, PacketOutMessage.class),
	DISCONNECT(0xE, 28, PacketOutDisconnect.class),

	EXT_INFO(0x10, 66, PacketOutExtInfo.class),
	EXT_ENTRY(0x11, 68, PacketOutExtEntry.class),
	CUSTOM_BLOCKS(0x13, 1, PacketOutCustomBlocks.class),
	BLOCK_PERMISSIONS(0x1C, 3, PacketOutBlockPermissions.class),
	BLOCK_DEFINITION(0x23, 79, PacketOutBlockDefinition.class),
	REMOVE_BLOCK_DEFINITION(0x24, 1, PacketOutRemoveBlockDefinition.class),
	ENV_MAP_APPEARANCE(0x1E, 72, PacketOutEnvMapAppearance.class),
	HACK_CONTROL(0x20, 7, PacketOutHackControl.class),
	BULK_BLOCK_CHANGE(0x26, 1282, PacketOutBulkBlockChange.class);

	private byte id;
	private int size;
	private Class<? extends OutgoingPacket> referenceClass;

	private PacketTypeOut(int id, int size, Class<? extends OutgoingPacket> referenceClass) {
	    this.id = (byte) id;
	    this.size = size;
	    this.referenceClass = referenceClass;
	}

	@Override
	public byte getId() {
	    return id;
	}

	@Override
	public int getSize() {
	    return size;
	}

	@Override
	public Class<? extends OutgoingPacket> getDefiningClass() {
	    return referenceClass;
	}
    }

    byte getId();

    int getSize();

    Class<? extends Packet> getDefiningClass();
}
