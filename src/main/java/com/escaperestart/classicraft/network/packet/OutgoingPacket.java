package com.escaperestart.classicraft.network.packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFutureListener;

public abstract class OutgoingPacket extends Packet {

    public OutgoingPacket(PacketType.PacketTypeOut type) {
	super(type);
    }

    public abstract void serializeData(ByteBuf buf) throws Exception;

    public final QueuedOutgoingPacket queue() {
	return queue(null);
    }

    public final QueuedOutgoingPacket queue(ChannelFutureListener future) {
	return new QueuedOutgoingPacket(future);
    }

    public final class QueuedOutgoingPacket {

	private final Packet packet;
	private final ChannelFutureListener future;

	private QueuedOutgoingPacket(ChannelFutureListener future) {
	    this.packet = OutgoingPacket.this;
	    this.future = future;
	}

	public Packet getPacket() {
	    return packet;
	}

	public ChannelFutureListener getFutureListener() {
	    return future;
	}

	public boolean hasFutureListener() {
	    return future != null;
	}
    }
}
