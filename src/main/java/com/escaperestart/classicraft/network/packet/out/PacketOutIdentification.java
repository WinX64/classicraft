package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;
import com.escaperestart.classicraft.player.UserType;

import io.netty.buffer.ByteBuf;

public class PacketOutIdentification extends OutgoingPacket {

    private byte protocolVersion;
    private String serverName;
    private String serverMotd;
    private UserType userType;

    public PacketOutIdentification(byte protocolVersion, String serverName, String serverMotd, UserType userType) {
	super(PacketType.PacketTypeOut.IDENTIFICATION);
	this.protocolVersion = protocolVersion;
	this.serverName = serverName;
	this.serverMotd = serverMotd;
	this.userType = userType;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(protocolVersion);
	buf.writeBytes(PacketEncoder.encodeString(serverName));
	buf.writeBytes(PacketEncoder.encodeString(serverMotd));
	buf.writeByte(userType.getId());
    }
}
