package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutPositionOrientation extends OutgoingPacket {

    private final byte playerId;
    private final byte changePosX;
    private final byte changePosY;
    private final byte changePosZ;
    private final byte yaw;
    private final byte pitch;

    public PacketOutPositionOrientation(byte playerId, byte changePosX, byte changePosY, byte changePosZ, byte yaw,
	    byte pitch) {
	super(PacketType.PacketTypeOut.POSTION_ORIENTATION);
	this.playerId = playerId;
	this.changePosX = changePosX;
	this.changePosY = changePosY;
	this.changePosZ = changePosZ;
	this.yaw = yaw;
	this.pitch = pitch;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(playerId);
	buf.writeByte(changePosX);
	buf.writeByte(changePosY);
	buf.writeByte(changePosZ);
	buf.writeByte(yaw);
	buf.writeByte(pitch);
    }
}
