package com.escaperestart.classicraft.network.packet.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.io.PlayerAuthenticator;
import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.in.PacketInBlockChange;
import com.escaperestart.classicraft.network.packet.in.PacketInCustomBlocks;
import com.escaperestart.classicraft.network.packet.in.PacketInExtEntry;
import com.escaperestart.classicraft.network.packet.in.PacketInExtInfo;
import com.escaperestart.classicraft.network.packet.in.PacketInIdentification;
import com.escaperestart.classicraft.network.packet.in.PacketInMessage;
import com.escaperestart.classicraft.network.packet.in.PacketInPositionOrientation;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;
import com.escaperestart.classicraft.network.packet.out.PacketOutExtEntry;
import com.escaperestart.classicraft.network.packet.out.PacketOutExtInfo;
import com.escaperestart.classicraft.network.packet.out.PacketOutIdentification;
import com.escaperestart.classicraft.network.packet.out.PacketOutMessage;
import com.escaperestart.classicraft.player.MessageType;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.util.Util;

public class LoginPacketHandler extends PacketHandler {

    private static final Logger LOGGER = LogManager.getLogger();
    
    private enum LoginState {
	AWAITING_IDENTIFICATION,
	IDENTIFYING,
	IDENTIFIED,
	AWAITING_EXTENSIONS,
	READY
    }

    private LoginState state;

    private String finalName;
    private boolean extendedProtocol;
    private String clientName;
    private int clientExtensions;

    public LoginPacketHandler(Server server, ClientConnection clientConnection) {
	super(server, clientConnection);
	this.state = LoginState.AWAITING_IDENTIFICATION;

	this.finalName = null;
	this.extendedProtocol = false;
	this.clientName = "Vanilla";
	this.clientExtensions = -1;
    }

    @Override
    public void tick() {
	if (currentTick > 30) {
	    clientConnection.close("Took too long to log-in!", ChatColor.RED + "Took too long to log-in!");
	    return;
	}
	switch (state) {
	    case AWAITING_IDENTIFICATION:
		break;

	    case IDENTIFYING:
		break;

	    case IDENTIFIED:
		if (extendedProtocol) {
		    this.clientConnection.sendPacket(new PacketOutExtInfo("ClassiCraft", 6), null);
		    this.clientConnection.sendPacket(new PacketOutExtEntry("CustomBlocks", 1), null);
		    this.clientConnection.sendPacket(new PacketOutExtEntry("BlockDefinitions", 1), null);
		    this.clientConnection.sendPacket(new PacketOutExtEntry("EnvMapAppearance", 2), null);
		    this.clientConnection.sendPacket(new PacketOutExtEntry("MessageTypes", 1), null);
		    this.clientConnection.sendPacket(new PacketOutExtEntry("HackControl", 1), null);
		    this.clientConnection.sendPacket(new PacketOutExtEntry("BlockPermissions", 1), null);
		    this.state = LoginState.AWAITING_EXTENSIONS;
		} else {
		    this.state = LoginState.READY;
		}
		break;

	    case AWAITING_EXTENSIONS:
		break;

	    case READY: {
		Player samePlayer = server.getPlayerExact(finalName);
		if (samePlayer != null) {
		    samePlayer.kick("Logged-in from another location!",
			    ChatColor.RED + "Logged-in from another location!");
		    break;
		}

		byte freeId = server.getFreeId();
		if (freeId == Player.SELF) {
		    clientConnection.close("No free IDs left!", ChatColor.RED + "Server player limit reached!");
		    return;
		}

		Player player = new Player(server, finalName, clientConnection);
		player.setId(freeId);
		player.setOp(finalName.equals("WinX64"));
		player.setClientName(clientName);
		// clientConnection.sendPacket(new PacketOutCustomBlocks(1),
		// null);
		clientConnection.sendPacket(new PacketOutIdentification(Server.PROTOCOL_VERSION, server.getServerName(),
			server.getServerMotd(), player.getUserType()), null);
		clientConnection.setPacketHandler(new GamePacketHandler(server, clientConnection, player));
		server.loginPlayer(player);
		server.getMainWorld().join(player);
		server.broadcastMessage(ChatColor.GREEN + "+ " + player.getColoredName() + ChatColor.YELLOW
			+ " joined the server with " + clientName + "!");
		if (this.extendedProtocol) {
		    clientConnection
			    .sendPacket(
				    new PacketOutMessage(MessageType.UPPER_ONE, ChatColor.RED + "Lava" + ChatColor.WHITE
					    + ": 5:00  " + ChatColor.GRAY + "Round End" + ChatColor.WHITE + ": 15:00"),
				    null);
		    clientConnection.sendPacket(
			    new PacketOutMessage(MessageType.ANNOUNCEMENT, ChatColor.GREEN + "Welcome! :D"), null);
		}
		break;
	    }
	}
	this.currentTick++;
    }

    @Override
    public void disconnect(String reason) {}

    @Override
    public void processIdentification(PacketInIdentification packet) {
	if (state != LoginState.AWAITING_IDENTIFICATION) {
	    return;
	}
	this.state = LoginState.IDENTIFYING;

	if (packet.getMagicalKey() == 0x42) {
	    this.extendedProtocol = true;
	}

	if (packet.getPlayerName().length() > 16) {
	    clientConnection.close("Name too long",
		    ChatColor.RED + "Your name is too long! It can be at most 16 characters.");
	    return;
	}

	if (!Util.isValidPlayerName(packet.getPlayerName())) {
	    clientConnection.close("Invalid name", ChatColor.RED + "Your name contains invalid characters!");
	    return;
	}

	if (packet.getProtocolVersion() != Server.PROTOCOL_VERSION) {
	    clientConnection.close("Wrong version",
		    String.format(ChatColor.RED + "Wrong protocol version! Use version %d to play in here!",
			    Server.PROTOCOL_VERSION));
	    return;
	}

	if (!server.getTicker().isAcceptingNewConnections()) {
	    clientConnection.close("Too many connections!",
		    ChatColor.RED + "Too many connections! Try again in some minutes.");
	    return;
	}

	PlayerAuthenticator auth = new PlayerAuthenticator(server, clientConnection, packet, this);
	server.getTicker().startAuthentication(auth);
	auth.start();
    }

    @Override
    public void processExtInfo(PacketInExtInfo packet) {
	if (state != LoginState.AWAITING_EXTENSIONS) {
	    return;
	}

	if (this.clientExtensions != -1) {
	    throw new IllegalStateException("Illegal packet! Already received extensio info!");
	}

	this.clientName = packet.getAppName();
	this.clientExtensions = packet.getExtensions();
    }

    @Override
    public void processExtEntry(PacketInExtEntry packet) {
	if (state != LoginState.AWAITING_EXTENSIONS) {
	    return;
	}

	LOGGER.info("ExtName: {}, Version {}", packet.getExtensionName(), packet.getVersion());
	this.clientExtensions--;
	if (clientExtensions == 0) {
	    this.state = LoginState.READY;
	} else if (clientExtensions < 0) {
	    throw new IllegalStateException("Illegal packet! Too many extensions!");
	}
    }

    @Override
    public void processBlockChange(PacketInBlockChange packet) {
	throw new IllegalStateException("Illegal packet! Still not logged in!");
    }

    @Override
    public void processMessage(PacketInMessage packet) {
	throw new IllegalStateException("Illegal packet! Still not logged in!");
    }

    @Override
    public void processPositionOrientation(PacketInPositionOrientation packet) {
	throw new IllegalStateException("Illegal packet! Still not logged in!");
    }

    @Override
    public void processCustomBlocks(PacketInCustomBlocks packet) {
	LOGGER.info("Block support level: {}", packet.getSupportLevel());
    }

    public void completeAuthentication(String finalName) {
	this.finalName = finalName;
	this.state = LoginState.IDENTIFIED;
    }
}
