package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;

import io.netty.buffer.ByteBuf;

public class PacketOutExtInfo extends OutgoingPacket {

    private final String appName;
    private final int extensions;

    public PacketOutExtInfo(String appName, int extensions) {
	super(PacketType.PacketTypeOut.EXT_INFO);
	this.appName = appName;
	this.extensions = extensions;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeBytes(PacketEncoder.encodeString(appName));
	buf.writeShort(extensions);
    }
}
