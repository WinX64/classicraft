package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutPing extends OutgoingPacket {

    public PacketOutPing() {
	super(PacketType.PacketTypeOut.PING);
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {}
}
