package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;

import io.netty.buffer.ByteBuf;

public class PacketOutSpawnPlayer extends OutgoingPacket {

    private byte playerId;
    private String playerName;
    private short posX;
    private short posY;
    private short posZ;
    private byte yaw;
    private byte pitch;

    public PacketOutSpawnPlayer(byte playerId, String playerName, short posX, short posY, short posZ, byte yaw,
	    byte pitch) {
	super(PacketType.PacketTypeOut.SPAWN_PLAYER);
	this.playerId = playerId;
	this.playerName = playerName;
	this.posX = posX;
	this.posY = posY;
	this.posZ = posZ;
	this.yaw = yaw;
	this.pitch = pitch;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(playerId);
	buf.writeBytes(PacketEncoder.encodeString(playerName));
	buf.writeShort(posX);
	buf.writeShort(posY);
	buf.writeShort(posZ);
	buf.writeByte(yaw);
	buf.writeByte(pitch);
    }
}
