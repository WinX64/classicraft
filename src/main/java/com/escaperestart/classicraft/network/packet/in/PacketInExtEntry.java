package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketDecoder;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInExtEntry extends IncomingPacket {

    private String extName;
    private int version;

    public PacketInExtEntry() {
	super(PacketType.PacketTypeIn.EXT_ENTRY);
    }

    public String getExtensionName() {
	return extName;
    }

    public int getVersion() {
	return version;
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.extName = PacketDecoder.decodeString(buf);
	this.version = buf.readInt();
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processExtEntry(this);
    }
}
