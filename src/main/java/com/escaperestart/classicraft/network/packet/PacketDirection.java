package com.escaperestart.classicraft.network.packet;

public enum PacketDirection {

    IN,
    OUT
}
