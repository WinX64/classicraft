package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;

import io.netty.buffer.ByteBuf;

public class PacketOutExtEntry extends OutgoingPacket {

    private final String extName;
    private final int version;

    public PacketOutExtEntry(String extName, int version) {
	super(PacketType.PacketTypeOut.EXT_ENTRY);
	this.extName = extName;
	this.version = version;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeBytes(PacketEncoder.encodeString(extName));
	buf.writeInt(version);
    }
}
