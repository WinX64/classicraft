package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutBulkBlockChange extends OutgoingPacket {

    private final byte changes;
    private final int[] positions;
    private final byte[] blocks;

    public PacketOutBulkBlockChange(int changes, int[] positions, byte[] blocks) {
	super(PacketType.PacketTypeOut.BULK_BLOCK_CHANGE);
	this.changes = (byte) (changes - 1);
	this.positions = positions;
	this.blocks = blocks;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(changes);
	for (int position : positions) {
	    buf.writeInt(position);
	}
	buf.writeBytes(blocks);
    }
}
