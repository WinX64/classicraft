package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.block.Block;
import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutBlockChange extends OutgoingPacket {

    private short blockX;
    private short blockY;
    private short blockZ;
    private byte blockId;

    public PacketOutBlockChange(short blockX, short blockY, short blockZ, Block block) {
	this(blockX, blockY, blockZ, block.getId());
    }

    public PacketOutBlockChange(short blockX, short blockY, short blockZ, byte blockId) {
	super(PacketType.PacketTypeOut.BLOCK_CHANGE);
	this.blockX = blockX;
	this.blockY = blockY;
	this.blockZ = blockZ;
	this.blockId = blockId;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeShort(blockX);
	buf.writeShort(blockY);
	buf.writeShort(blockZ);
	buf.writeByte(blockId);
    }
}
