package com.escaperestart.classicraft.network.packet.in;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;

import io.netty.buffer.ByteBuf;

public class PacketInPositionOrientation extends IncomingPacket {

    private byte playerId;
    private short posX;
    private short posY;
    private short posZ;
    private byte yaw;
    private byte pitch;

    public PacketInPositionOrientation() {
	super(PacketType.PacketTypeIn.POSITION_ORIENTATION);
    }

    @Override
    public void deserializeData(ByteBuf buf) throws Exception {
	this.playerId = buf.readByte();
	this.posX = buf.readShort();
	this.posY = buf.readShort();
	this.posZ = buf.readShort();
	this.yaw = buf.readByte();
	this.pitch = buf.readByte();
    }

    @Override
    public void processPacket(PacketHandler handler) throws Exception {
	handler.processPositionOrientation(this);
    }

    public byte getPlayerId() {
	return playerId;
    }

    public short getPosX() {
	return posX;
    }

    public short getPosY() {
	return posY;
    }

    public short getPosZ() {
	return posZ;
    }

    public byte getYaw() {
	return yaw;
    }

    public byte getPitch() {
	return pitch;
    }
}
