package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutHackControl extends OutgoingPacket {

    private final boolean allowFly;
    private final boolean allowNoClip;
    private final boolean allowSpeed;
    private final boolean allowRespawn;
    private final boolean allowThirdPerson;
    private final int maxJumpHeight;

    public PacketOutHackControl(boolean allowFly, boolean allowNoClip, boolean allowSpeed, boolean allowRespawn,
	    boolean allowThirdPerson, int maxJumpHeight) {
	super(PacketType.PacketTypeOut.HACK_CONTROL);
	this.allowFly = allowFly;
	this.allowNoClip = allowNoClip;
	this.allowSpeed = allowSpeed;
	this.allowRespawn = allowRespawn;
	this.allowThirdPerson = allowThirdPerson;
	this.maxJumpHeight = maxJumpHeight;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeBoolean(allowFly);
	buf.writeBoolean(allowNoClip);
	buf.writeBoolean(allowSpeed);
	buf.writeBoolean(allowRespawn);
	buf.writeBoolean(allowThirdPerson);
	buf.writeShort(maxJumpHeight);
    }
}
