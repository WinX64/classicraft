package com.escaperestart.classicraft.network.packet;

public abstract class Packet {

    public static final int STRING_SIZE = 64;
    public static final byte STRING_DELIMETER = (byte) ' ';

    protected final PacketType type;

    Packet(PacketType type) {
	this.type = type;
    }

    public PacketType getPacketType() {
	return type;
    }
}
