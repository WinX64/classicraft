package com.escaperestart.classicraft.network.packet.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.network.ClientConnection;
import com.escaperestart.classicraft.network.packet.in.PacketInBlockChange;
import com.escaperestart.classicraft.network.packet.in.PacketInCustomBlocks;
import com.escaperestart.classicraft.network.packet.in.PacketInExtEntry;
import com.escaperestart.classicraft.network.packet.in.PacketInExtInfo;
import com.escaperestart.classicraft.network.packet.in.PacketInIdentification;
import com.escaperestart.classicraft.network.packet.in.PacketInMessage;
import com.escaperestart.classicraft.network.packet.in.PacketInPositionOrientation;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

public class GamePacketHandler extends PacketHandler {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String PLAYER_MESSAGE_FORMAT = "<%s> %s";

    private Player player;

    private int timeoutCounter;

    public GamePacketHandler(Server server, ClientConnection clientConnection, Player player) {
	super(server, clientConnection);
	this.player = player;

	this.timeoutCounter = 0;
    }

    @Override
    public void tick() {
	if (timeoutCounter > 600) {
	    clientConnection.close("Read timeout!", ChatColor.RED + "Read timeout!");
	    return;
	}
	this.timeoutCounter++;
	this.currentTick++;
    }

    @Override
    public void disconnect(String reason) {
	LOGGER.info("{} has disconnected: {}", player.getName(), reason);
	server.broadcastMessage(
		ChatColor.RED + "- " + player.getColoredName() + ChatColor.YELLOW + " quit the server!");
	server.logoutPlayer(player);
	player.getWorld().quit(player);
    }

    @Override
    public void processIdentification(PacketInIdentification packet) {
	throw new IllegalStateException("Illegal packet! Already logged in!");
    }

    @Override
    public void processExtInfo(PacketInExtInfo packet) {
	throw new IllegalStateException("Illegal packet! Already logged in!");
    }

    @Override
    public void processExtEntry(PacketInExtEntry packet) {
	throw new IllegalStateException("Illegal packet! Already logged in!");
    }

    @Override
    public void processCustomBlocks(PacketInCustomBlocks packet) {
	LOGGER.info("Block support level: {}", packet.getSupportLevel());
    }

    @Override
    public void processBlockChange(PacketInBlockChange packet) {
	this.timeoutCounter = 0;
	if (packet.getAction() == 0) {
	    player.getWorld().breakBlock(player, packet.getBlockX(), packet.getBlockY(), packet.getBlockZ());
	} else {
	    player.getWorld().buildBlock(player, packet.getBlockX(), packet.getBlockY(), packet.getBlockZ(),
		    packet.getNewBlock());
	}
    }

    @Override
    public void processMessage(PacketInMessage packet) {
	this.timeoutCounter = 0;
	char zero = packet.getMessage().charAt(0);
	if (zero == '/' || zero == '!') {
	    player.executeCommand(packet.getMessage().substring(1));
	} else {
	    player.getWorld()
		    .broadcastMessage(String.format(PLAYER_MESSAGE_FORMAT, player.getColoredName() + ChatColor.WHITE,
			    ChatColor.translateAlternateColorCode('%', packet.getMessage())));
	}
    }

    @Override
    public void processPositionOrientation(PacketInPositionOrientation packet) {
	this.timeoutCounter = 0;
	if (player.matchesLastTeleport(packet)) {
	    return;
	}
	if (!player.noClipAllowed()
		&& !player.getWorld().isValidPosition(player, packet.getPosX(), packet.getPosY(), packet.getPosZ())) {
	    player.sendTeleport(player, true, true);
	    return;
	}
	if (packet.getPosY() < player.getPosY()) {
	    double yDist = Math.abs(player.getPosY() - packet.getPosY());
	    if (yDist > 16900) {
		player.sendTeleport(player, true, true);
		return;
	    }
	    double dist = Math.pow(player.getPosX() - packet.getPosX(), 2)
		    + Math.pow(player.getPosZ() - packet.getPosZ(), 2);
	    if (dist > 1024) {
		// player.sendTeleport(player, true, true);
		// return;
	    }
	} else {
	    double dist = Math.pow(player.getPosX() - packet.getPosX(), 2)
		    + Math.pow(player.getPosY() - packet.getPosY(), 2)
		    + Math.pow(player.getPosZ() - packet.getPosZ(), 2);
	    if (dist > 1024) {
		// player.sendTeleport(player, true, true);
		// return;
	    }

	    // TODO Fix /tp and player's collision box (eye location)
	}
	player.setPositionOrientation(packet.getPosX(), packet.getPosY(), packet.getPosZ(), packet.getYaw(),
		packet.getPitch());
	// player.sendMessage(String.format("%d, %d, %d", player.getPosX(),
	// player.getPosY(), player.getPosZ()));

	for (Player target : player.getWorld().getPlayers()) {
	    if (target == player) {
		continue;
	    }
	    player.sendTeleport(target, false, false);
	}
    }
}
