package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutLevelFinalize extends OutgoingPacket {

    private final short width;
    private final short height;
    private final short length;

    public PacketOutLevelFinalize(int width, int height, int length) {
	super(PacketType.PacketTypeOut.LEVEL_FINALIZE);
	this.width = (short) width;
	this.height = (short) height;
	this.length = (short) length;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeShort(width);
	buf.writeShort(height);
	buf.writeShort(length);
    }
}
