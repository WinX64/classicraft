package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;

import io.netty.buffer.ByteBuf;

public class PacketOutBlockPermissions extends OutgoingPacket {

    private final byte blockId;
    private final boolean allowPlace;
    private final boolean allowBreak;

    public PacketOutBlockPermissions(int blockId, boolean allowPlace, boolean allowBreak) {
	super(PacketType.PacketTypeOut.BLOCK_PERMISSIONS);
	this.blockId = (byte) blockId;
	this.allowPlace = allowPlace;
	this.allowBreak = allowBreak;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(blockId);
	buf.writeBoolean(allowPlace);
	buf.writeBoolean(allowBreak);
    }
}
