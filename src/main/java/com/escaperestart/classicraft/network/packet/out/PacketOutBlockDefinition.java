package com.escaperestart.classicraft.network.packet.out;

import com.escaperestart.classicraft.block.BlockModel;
import com.escaperestart.classicraft.block.BlockSolidity;
import com.escaperestart.classicraft.block.BlockSound;
import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.PacketType;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;
import com.escaperestart.classicraft.util.Color;

import io.netty.buffer.ByteBuf;

public class PacketOutBlockDefinition extends OutgoingPacket {

    private final byte blockId;
    private final String blockName;
    private final BlockSolidity solidity;
    private final byte movementSpeed;
    private final byte textureTopId;
    private final byte textureSideId;
    private final byte textureBottomId;
    private final boolean translucent;
    private final BlockSound sound;
    private final boolean fullBright;
    private final byte shape;
    private final BlockModel model;
    private final byte fogDensity;
    private final Color fogColor;

    public PacketOutBlockDefinition(int blockId, String blockName, BlockSolidity solidity, int movementSpeed,
	    int textureTopId, int textureSideId, int textureBottomId, boolean translucent, BlockSound sound,
	    boolean fullBright, int shape, BlockModel model, int fogDensity, Color fogColor) {
	super(PacketType.PacketTypeOut.BLOCK_DEFINITION);
	this.blockId = (byte) blockId;
	this.blockName = blockName;
	this.solidity = solidity;
	this.movementSpeed = (byte) movementSpeed;
	this.textureTopId = (byte) textureTopId;
	this.textureSideId = (byte) textureSideId;
	this.textureBottomId = (byte) textureBottomId;
	this.translucent = translucent;
	this.sound = sound;
	this.fullBright = fullBright;
	this.shape = (byte) shape;
	this.model = model;
	this.fogDensity = (byte) fogDensity;
	this.fogColor = fogColor;
    }

    @Override
    public void serializeData(ByteBuf buf) throws Exception {
	buf.writeByte(blockId);
	buf.writeBytes(PacketEncoder.encodeString(blockName));
	buf.writeByte(solidity.ordinal());
	buf.writeByte(movementSpeed);
	buf.writeByte(textureTopId);
	buf.writeByte(textureSideId);
	buf.writeByte(textureBottomId);
	buf.writeBoolean(translucent);
	buf.writeByte(sound.ordinal());
	buf.writeBoolean(fullBright);
	buf.writeByte(shape);
	buf.writeByte(model.ordinal());
	buf.writeByte(fogDensity);
	buf.writeByte(fogColor.getRed());
	buf.writeByte(fogColor.getGreen());
	buf.writeByte(fogColor.getBlue());
    }
}
