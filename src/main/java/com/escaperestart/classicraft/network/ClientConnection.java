package com.escaperestart.classicraft.network;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.network.packet.IncomingPacket;
import com.escaperestart.classicraft.network.packet.OutgoingPacket;
import com.escaperestart.classicraft.network.packet.OutgoingPacket.QueuedOutgoingPacket;
import com.escaperestart.classicraft.network.packet.io.PacketHandler;
import com.escaperestart.classicraft.network.packet.out.PacketOutDisconnect;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ClientConnection extends SimpleChannelInboundHandler<IncomingPacket> {

    private static final Logger LOGGER = LogManager.getLogger();

    private final Channel channel;

    private PacketHandler packetHandler;

    private final Queue<IncomingPacket> queuedIncomingPackets;
    private final Queue<QueuedOutgoingPacket> queuedOutgoingPackets;

    private Player player;

    private String disconnectionReason;

    public ClientConnection(Server server, Channel channel) {
	this.channel = channel;

	this.queuedIncomingPackets = new ConcurrentLinkedQueue<IncomingPacket>();
	this.queuedOutgoingPackets = new ConcurrentLinkedQueue<QueuedOutgoingPacket>();

	this.player = null;

	this.disconnectionReason = null;
    }

    public void tick() throws Exception {
	this.processIncomingPackets();
	this.processOutgoingPackets();
	this.packetHandler.tick();
    }

    public boolean isConnected() {
	return channel != null && channel.isOpen();
    }

    public Player getPlayer() {
	return player;
    }

    public void setPacketHandler(PacketHandler packetHandler) {
	this.packetHandler = packetHandler;
    }

    public void registerPlayer(Player player) {
	if (this.player == null) {
	    this.player = player;
	}
    }

    public void queueIncomingPacket(IncomingPacket packet) {
	synchronized (queuedIncomingPackets) {
	    queuedIncomingPackets.add(packet);
	}
    }

    public void sendPacket(final OutgoingPacket packet, final ChannelFutureListener postAction) {
	if (!this.isConnected()) {
	    return;
	}

	// TODO Rework here. What is the best way to handle outgoing packets?

	synchronized (this.queuedOutgoingPackets) {
	    this.queuedOutgoingPackets.add(packet.queue(postAction));
	}

	/*
	 * if (channel.eventLoop().inEventLoop()) { ChannelFuture future =
	 * channel.writeAndFlush(packet); if (postAction != null) {
	 * future.addListener(postAction); } } else {
	 * channel.eventLoop().execute(new Runnable() {
	 * 
	 * @Override public void run() { ChannelFuture future =
	 * channel.writeAndFlush(packet); if (postAction != null) {
	 * future.addListener(postAction); } }
	 * 
	 * }); }
	 */
    }

    public Channel getChannel() {
	return channel;
    }

    public void close(String reason) {
	close(reason, null);
    }

    public void close(String reason, String disconnectionMessage) {
	if (!channel.config().isAutoRead()) {
	    return;
	}

	this.disconnectionReason = reason;
	this.channel.config().setAutoRead(false);
	sendPacket(new PacketOutDisconnect(disconnectionMessage), ChannelFutureListener.CLOSE);
    }

    public void handleDisconnect() {
	this.packetHandler.disconnect(disconnectionReason);
	LOGGER.info("{} disconnected!", channel.remoteAddress());
    }

    private void processIncomingPackets() throws Exception {
	synchronized (queuedIncomingPackets) {
	    while (!queuedIncomingPackets.isEmpty()) {
		queuedIncomingPackets.poll().processPacket(packetHandler);
	    }
	}
    }

    private void processOutgoingPackets() throws Exception {
	synchronized (queuedOutgoingPackets) {
	    while (!queuedOutgoingPackets.isEmpty()) {
		QueuedOutgoingPacket queuedPacket = queuedOutgoingPackets.poll();
		ChannelFuture future = this.channel.write(queuedPacket.getPacket());
		LOGGER.debug("{} sent to {}", queuedPacket.getPacket().getClass().getSimpleName(),
			player == null ? this.channel.remoteAddress() : player.getName());
		if (queuedPacket.hasFutureListener()) {
		    future.addListener(queuedPacket.getFutureListener());
		}
	    }
	    this.channel.flush();
	}
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket msg) throws Exception {
	this.queueIncomingPacket(msg);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
	this.close(cause.getMessage(), ChatColor.RED + cause.toString());
    }
}
