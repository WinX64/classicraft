package com.escaperestart.classicraft.network;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

public class CloseClientFuture implements ChannelFutureListener {

    private ClientConnection clientConnection;
    
    public CloseClientFuture(ClientConnection clientConnection) {
	this.clientConnection = clientConnection;
    }
    
    @Override
    public void operationComplete(ChannelFuture paramF) throws Exception {
	this.clientConnection.close(null);
    }
}
