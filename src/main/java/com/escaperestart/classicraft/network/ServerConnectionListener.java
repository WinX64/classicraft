package com.escaperestart.classicraft.network;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.network.packet.handler.LoginPacketHandler;
import com.escaperestart.classicraft.network.packet.io.PacketDecoder;
import com.escaperestart.classicraft.network.packet.io.PacketEncoder;
import com.escaperestart.classicraft.network.packet.out.PacketOutDisconnect;
import com.escaperestart.classicraft.server.Server;
import com.escaperestart.classicraft.util.ChatColor;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;

public class ServerConnectionListener extends ChannelInitializer<Channel> {

	private static final Logger LOGGER = LogManager.getLogger();

	private Server server;

	private List<ClientConnection> clientConnections;

	public ServerConnectionListener(Server server) {
		this.server = server;

		this.clientConnections = new ArrayList<ClientConnection>();
	}

	@Override
	protected void initChannel(Channel channel) throws Exception {
		ClientConnection clientConnection = new ClientConnection(server, channel);
		channel.config().setOption(ChannelOption.TCP_NODELAY, true);
		channel.pipeline().addLast("packetEncoder", new PacketEncoder(server, clientConnection))
				.addLast("packerDecoder", new PacketDecoder(server, clientConnection))
				.addLast("packetHandler", clientConnection);
		clientConnection.setPacketHandler(new LoginPacketHandler(server, clientConnection));
		synchronized (clientConnections) {
			clientConnections.add(clientConnection);
		}
		LOGGER.info("{} has connected!", channel.remoteAddress());
	}

	public void tickConnections() {
		synchronized (clientConnections) {
			Iterator<ClientConnection> iter = clientConnections.iterator();
			while (iter.hasNext()) {
				ClientConnection clientConnection = iter.next();
				if (clientConnection.isConnected()) {
					try {
						clientConnection.tick();
					} catch (Exception e) {
						LOGGER.error("Failed to tick connection for {}. Details below:",
								clientConnection.getChannel().remoteAddress());
						e.printStackTrace();
						clientConnection.close("Internal exception: " + e.getMessage(), ChatColor.RED + e.getMessage());
					}
				} else {
					iter.remove();
					clientConnection.handleDisconnect();
				}
			}
		}
	}

	public void stop(String reason) {
		synchronized (clientConnections) {
			PacketOutDisconnect packet = new PacketOutDisconnect(reason);
			Iterator<ClientConnection> iter = clientConnections.iterator();
			while (iter.hasNext()) {
				ClientConnection clientConnection = iter.next();
				clientConnection.sendPacket(packet, ChannelFutureListener.CLOSE);
				clientConnection.getChannel().closeFuture().syncUninterruptibly();
				iter.remove();
			}
		}
	}
}
