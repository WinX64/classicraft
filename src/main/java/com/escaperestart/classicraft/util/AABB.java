package com.escaperestart.classicraft.util;

public final class AABB {

    public static final AABB EMPTY = new AABB(0, 0, 0);
    public static final AABB BLOCK = new AABB(0, 0, 0, 31, 31, 31);
    public static final AABB HALF_BLOCK = new AABB(0, 0, 0, 31, 15, 31);
    public static final AABB PLAYER = new AABB(-8, -51, -8, 8, 0, 8);

    private final short startX;
    private final short startY;
    private final short startZ;
    private final short endX;
    private final short endY;
    private final short endZ;

    public AABB() {
	this(0, 0, 0);
    }

    public AABB(int startX, int startY, int startZ) {
	this(startX, startY, startZ, 0);
    }

    public AABB(int startX, int startY, int startZ, int increment) {
	this(startX, startY, startZ, startX + increment, startY + increment, startZ + increment);
    }

    public AABB(int startX, int startY, int startZ, int endX, int endY, int endZ) {
	this.startX = (short) Math.min(startX, endX);
	this.startY = (short) Math.min(startY, endY);
	this.startZ = (short) Math.min(startZ, endZ);
	this.endX = (short) Math.max(startX, endX);
	this.endY = (short) Math.max(startY, endY);
	this.endZ = (short) Math.max(startZ, endZ);
    }

    public short getStartX() {
	return startX;
    }

    public short getBlockStartX() {
	return (short) (startX >> 5);
    }

    public short getStartY() {
	return startY;
    }

    public short getBlockStartY() {
	return (short) (startY >> 5);
    }

    public short getStartZ() {
	return startZ;
    }

    public short getBlockStartZ() {
	return (short) (startZ >> 5);
    }

    public short getEndX() {
	return endX;
    }

    public short getBlockEndX() {
	return (short) (endX >> 5);
    }

    public short getEndY() {
	return endY;
    }

    public short getBlockEndY() {
	return (short) (endY >> 5);
    }

    public short getEndZ() {
	return endZ;
    }

    public short getBlockEndZ() {
	return (short) (endZ >> 5);
    }

    public AABB toBlockPosition(int x, int y, int z) {
	return this.move(x * 32, y * 32, z * 32);
    }

    public boolean intersect(AABB box) {
	return startX <= box.endX && endX >= box.startX && startY <= box.endY && endY >= box.startY
		&& startZ <= box.endZ && endZ >= box.startZ;
    }

    public AABB move(int diffX, int diffY, int diffZ) {
	return new AABB(startX + diffX, startY + diffY, startZ + diffZ, endX + diffX, endY + diffY, endZ + diffZ);
    }

    public AABB shrink(int x, int y, int z) {
	return shrink(x, y, z, x, y, z);
    }

    public AABB shrink(int xOne, int yOne, int zOne, int xTwo, int yTwo, int zTwo) {
	return new AABB(startX + xOne, startY + yOne, startZ + zOne, endX - xTwo, endY - yTwo, endZ - zTwo);
    }

    public AABB grow(int diffX, int diffY, int diffZ) {
	return new AABB(startX - diffX, startY - diffY, startZ - diffZ, endX + diffX, endY + diffY, endZ + diffZ);
    }

    public boolean isEmpty() {
	return startX == 0 && startY == 0 && startZ == 0 && endX == 0 && endY == 0 && endZ == 0;
    }

    @Override
    public String toString() {
	return String.format("[%d, %d, %d -> %d, %d, %d]", startX, startY, startZ, endX, endY, endZ);
    }
}
