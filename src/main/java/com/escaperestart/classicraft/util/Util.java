package com.escaperestart.classicraft.util;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import com.escaperestart.classicraft.player.MessageSanitizer;
import com.escaperestart.classicraft.player.Player;

public class Util {

    private static final Pattern INVALID_PATTERN = Pattern.compile("([^a-zA-Z0-9_]+)");
    private static final String VALID_CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

    private Util() {
	throw new AssertionError();
    }

    public static boolean isValidPlayerName(String text) {
	for (char c : text.toCharArray()) {
	    if (VALID_CHARACTERS.indexOf(c) == -1) {
		return false;
	    }
	}
	return true;
    }
    
    public static boolean isValidPlayerNameRegex(String text) {
	return !INVALID_PATTERN.matcher(text).find();
    }

    public static String stringJoint(String joint, String[] args) {
	StringBuilder builder = new StringBuilder();
	for (String arg : args) {
	    builder.append(joint).append(arg);
	}
	return builder.length() > 0 ? builder.substring(joint.length()) : builder.toString();
    }

    public static void sendMessage(Collection<Player> players, String message) {
	List<String> sanitized = MessageSanitizer.sanitizeChatMessage(message);
	for (Player player : players) {
	    player.sendRawMessage(sanitized);
	}
    }

    public static String getState(boolean state, boolean colored) {
	return state ? (colored ? ChatColor.GREEN : "") + "True" : (colored ? ChatColor.RED : "") + "False";
    }
    
    public static Util getUtil() {
	return new Util();
    }
}
