package com.escaperestart.classicraft.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public enum ChatColor {

    BLACK('0'),
    DARK_BLUE('1'),
    DARK_GREEN('2'),
    DARK_AQUA('3'),
    DARK_RED('4'),
    DARK_PURPLE('5'),
    GOLD('6'),
    GRAY('7'),
    DARK_GRAY('8'),
    BLUE('9'),
    GREEN('a'),
    AQUA('b'),
    RED('c'),
    LIGHT_PURPLE('d'),
    YELLOW('e'),
    WHITE('f');

    public static final char COLOR_CHAR = '&';
    public static final String VALID_COLORS;
    private static final String COLOR_CHAR_STR = String.valueOf(COLOR_CHAR);
    private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("(?i)" + COLOR_CHAR_STR + "[0-9A-F]");
    private static Map<Character, ChatColor> COLOR_BY_CHARACTER = new HashMap<Character, ChatColor>();

    static {
	String validColors = "";
	for (ChatColor color : values()) {
	    validColors += color.colorCode;
	    COLOR_BY_CHARACTER.put(color.colorCode, color);
	}
	VALID_COLORS = validColors;
    }

    private final char colorCode;

    private ChatColor(char colorCode) {
	this.colorCode = colorCode;
    }

    public char getColorCode() {
	return colorCode;
    }

    @Override
    public final String toString() {
	return COLOR_CHAR_STR + colorCode;
    }

    public static ChatColor getColor(char colorChar) {
	return COLOR_BY_CHARACTER.get(colorChar);
    }

    public static String translateAlternateColorCode(char alternate, String text) {
	char[] c = text.toCharArray();
	for (int i = 0; i < c.length - 1; i++) {
	    if (c[i] == alternate && VALID_COLORS.indexOf(c[i + 1]) != -1) {
		c[i] = COLOR_CHAR;
		c[i + 1] = Character.toLowerCase(c[i + 1]);
	    }
	}
	return new String(c);
    }

    public static String stripColor(String text) {
	return STRIP_COLOR_PATTERN.matcher(text).replaceAll("");
    }
}
