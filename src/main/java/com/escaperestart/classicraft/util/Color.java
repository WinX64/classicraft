package com.escaperestart.classicraft.util;

public enum Color {

    BLACK(0, 0, 0),
    DARK_BLUE(0, 0, 191),
    DARK_GREEN(0, 191, 0),
    DARK_AQUA(0, 191, 191),
    DARK_RED(191, 0, 0),
    DARK_PURPLE(191, 0, 191),
    GOLD(191, 0, 191),
    GRAY(191, 191, 191),
    DARK_GRAY(64, 64, 64),
    BLUE(64, 64, 255),
    GREEN(64, 255, 64),
    AQUA(64, 255, 255),
    RED(255, 64, 64),
    LIGHT_PURPLE(255, 64, 255),
    YELLOW(255, 255, 64),
    WHITE(255, 255, 255);

    private final byte red;
    private final byte green;
    private final byte blue;

    private Color(int red, int green, int blue) {
	this.red = (byte) red;
	this.green = (byte) green;
	this.blue = (byte) blue;
    }

    public byte getRed() {
	return red;
    }

    public byte getGreen() {
	return green;
    }

    public byte getBlue() {
	return blue;
    }

    public int getRGB() {
	return (red << 16) | (green << 8) | blue;
    }
}
