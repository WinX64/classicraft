package com.escaperestart.classicraft.server;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.io.PlayerAuthenticator;
import com.escaperestart.classicraft.world.World;

public class ServerTicker extends Thread {

    private static final Logger LOGGER = LogManager.getLogger();

    private Server server;
    private boolean running;
    private double tps;
    private int currentTick;

    private List<PlayerAuthenticator> pendingAuthentications;

    public ServerTicker(Server server) {
	this.server = server;
	this.running = false;
	this.tps = 0;
	this.currentTick = 0;

	this.pendingAuthentications = new ArrayList<PlayerAuthenticator>();

	this.setName("Server Thread");
    }

    public void stopTicker(String reason) {
	this.running = false;
	this.server.getConnectionListener().stop(reason);
    }

    public double getTps() {
	return tps;
    }

    public int getCurrentTick() {
	return currentTick;
    }

    public void startAuthentication(PlayerAuthenticator auth) {
	synchronized (pendingAuthentications) {
	    this.pendingAuthentications.add(auth);
	}
    }

    public void finishAuthentication(PlayerAuthenticator auth) {
	synchronized (pendingAuthentications) {
	    this.pendingAuthentications.remove(auth);
	}
    }

    public boolean isAcceptingNewConnections() {
	return pendingAuthentications.size() <= 5;
    }

    @Override
    public void run() {
	this.running = true;
	long lastTick = 0;
	long catchupTime = 0;

	try {
	    while (running) {
		long curTime = System.nanoTime();
		long wait = 50000000L - (curTime - lastTick) - catchupTime;
		if (wait > 0L) {
		    Thread.sleep(wait / 1000000L);
		    catchupTime = 0L;
		} else {
		    catchupTime = Math.min(1000000000L, Math.abs(wait));

		    tps = tps * 0.95D + 1.0E9D / (curTime - lastTick) * 0.05D;
		    lastTick = curTime;
		    tickWorlds();
		    processCommands();
		    server.getConnectionListener().tickConnections();
		    currentTick++;
		}
	    }
	} catch (Exception e) {
	    LOGGER.fatal("A fatal error occurred during the server tick! Details below:");
	    e.printStackTrace();
	    server.stop("Server tick error!");
	}
    }

    private void tickWorlds() {
	for (World world : server.getWorlds()) {
	    world.tick();
	}
    }

    private void processCommands() {
	server.getConsoleCommandExecutor().processCommands();
    }
}
