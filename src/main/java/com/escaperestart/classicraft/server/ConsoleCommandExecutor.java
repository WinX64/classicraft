package com.escaperestart.classicraft.server;

import static org.fusesource.jansi.Ansi.ansi;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fusesource.jansi.Ansi.Attribute;
import org.fusesource.jansi.Ansi.Color;

import com.escaperestart.classicraft.command.CommandExecutor;
import com.escaperestart.classicraft.util.ChatColor;

public class ConsoleCommandExecutor implements CommandExecutor {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final Map<ChatColor, String> CONSOLE_COLORS = new HashMap<ChatColor, String>();

    static {
	CONSOLE_COLORS.put(ChatColor.BLACK, ansi().a(Attribute.RESET).fg(Color.BLACK).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.DARK_BLUE, ansi().a(Attribute.RESET).fg(Color.BLUE).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.DARK_GREEN, ansi().a(Attribute.RESET).fg(Color.GREEN).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.DARK_AQUA, ansi().a(Attribute.RESET).fg(Color.CYAN).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.DARK_RED, ansi().a(Attribute.RESET).fg(Color.RED).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.DARK_PURPLE, ansi().a(Attribute.RESET).fg(Color.MAGENTA).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.GOLD, ansi().a(Attribute.RESET).fg(Color.YELLOW).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.GRAY, ansi().a(Attribute.RESET).fg(Color.WHITE).boldOff().toString());
	CONSOLE_COLORS.put(ChatColor.DARK_GRAY, ansi().a(Attribute.RESET).fg(Color.BLACK).bold().toString());
	CONSOLE_COLORS.put(ChatColor.BLUE, ansi().a(Attribute.RESET).fg(Color.BLUE).bold().toString());
	CONSOLE_COLORS.put(ChatColor.GREEN, ansi().a(Attribute.RESET).fg(Color.GREEN).bold().toString());
	CONSOLE_COLORS.put(ChatColor.AQUA, ansi().a(Attribute.RESET).fg(Color.CYAN).bold().toString());
	CONSOLE_COLORS.put(ChatColor.RED, ansi().a(Attribute.RESET).fg(Color.RED).bold().toString());
	CONSOLE_COLORS.put(ChatColor.LIGHT_PURPLE, ansi().a(Attribute.RESET).fg(Color.MAGENTA).bold().toString());
	CONSOLE_COLORS.put(ChatColor.YELLOW, ansi().a(Attribute.RESET).fg(Color.YELLOW).bold().toString());
	CONSOLE_COLORS.put(ChatColor.WHITE, ansi().a(Attribute.RESET).fg(Color.WHITE).bold().toString());
    }

    private Server server;

    private ReentrantLock commandLock;
    private Queue<String> consoleCommands;

    public ConsoleCommandExecutor(Server server) {
	this.server = server;
	this.commandLock = new ReentrantLock();
	this.consoleCommands = new ConcurrentLinkedQueue<String>();
    }

    public void queueCommand(String rawCommand) {
	commandLock.lock();
	try {
	    consoleCommands.offer(rawCommand);
	} finally {
	    commandLock.unlock();
	}
    }

    public void processCommands() {
	commandLock.lock();
	try {
	    while (!consoleCommands.isEmpty()) {
		executeCommand(consoleCommands.poll());
	    }
	} finally {
	    commandLock.unlock();
	}
    }

    @Override
    public String getName() {
	return "Console";
    }

    @Override
    public void sendMessage(String message) {
	if (server.getOptions().has("nojline")) {
	    message = ChatColor.stripColor(message);
	} else {
	    for (Entry<ChatColor, String> entry : CONSOLE_COLORS.entrySet()) {
		message = message.replace(entry.getKey().toString(), entry.getValue());
	    }
	}
	LOGGER.info(message);
    }

    @Override
    public void executeCommand(String rawCommand) {
	server.getCommandList().handleCommand(this, rawCommand);
    }

    @Override
    public boolean hasPermission(String permission) {
	return true;
    }
}
