package com.escaperestart.classicraft.server;

import java.io.PrintStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.escaperestart.classicraft.command.CommandList;
import com.escaperestart.classicraft.io.ConsoleReadHandler;
import com.escaperestart.classicraft.io.ConsoleWriteHandler;
import com.escaperestart.classicraft.logging.LoggingOutputStream;
import com.escaperestart.classicraft.network.ServerConnectionListener;
import com.escaperestart.classicraft.network.packet.out.PacketOutMessage;
import com.escaperestart.classicraft.permission.PermissionManager;
import com.escaperestart.classicraft.player.MessageSanitizer;
import com.escaperestart.classicraft.player.MessageType;
import com.escaperestart.classicraft.player.Player;
import com.escaperestart.classicraft.util.ChatColor;
import com.escaperestart.classicraft.world.World;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.ResourceLeakDetector;
import jline.ConsoleReader;
import joptsimple.OptionSet;

public final class Server {

	public static final byte PROTOCOL_VERSION = 7;
	private static final Logger LOGGER = LogManager.getRootLogger();
	private static final Server INSTANCE = new Server();

	private OptionSet options;
	private String serverName;
	private String serverMotd;
	private ServerTicker serverThread;
	private ServerConnectionListener serverConnectionListener;
	private ConsoleReadHandler consoleReader;
	private ConsoleWriteHandler consoleWriter;

	private World mainWorld;
	private List<World> worlds;
	private CommandList commandList;
	private PermissionManager permissionManager;
	private ConsoleCommandExecutor consoleCommandExecutor;

	private List<Player> players;

	private InetAddress address;
	private int port;
	private ServerBootstrap bootstrap;
	private EventLoopGroup eventLoopGroup;

	private Server() {
		this.serverName = ChatColor.RED + "[" + ChatColor.DARK_RED + "ClassiCraft" + ChatColor.RED + "] "
				+ ChatColor.YELLOW + "Test Server";
		this.serverMotd = ChatColor.GREEN + "Lots of awesome stuff! " + ChatColor.DARK_GREEN + "+hax";
		this.serverThread = new ServerTicker(this);
		this.serverConnectionListener = new ServerConnectionListener(this);

		this.worlds = new ArrayList<World>();
		this.worlds.add(new World(this, "trench", (short) 64, (short) 128, (short) 64, "flatland"));
		this.worlds.add(new World(this, "pixelart", (short) 128, (short) 128, (short) 128, "pixel"));
		this.worlds.add(new World(this, "trench_small", (short) 16, (short) 16, (short) 16, "trench"));
		this.worlds.add(new World(this, "flatland", (short) 64, (short) 64, (short) 64, "default"));
		this.worlds.add(new World(this, "pixelart_big", (short) 256, (short) 256, (short) 256, "pixel"));
		this.worlds.add(new World(this, "limit", (short) 1024, (short) 1024, (short) 1024, "pixel"));
		this.mainWorld = worlds.get(0);
		this.commandList = new CommandList(this);
		this.commandList.registerDefaults();
		this.permissionManager = new PermissionManager(this);
		this.permissionManager.registerDefaults();
		this.consoleCommandExecutor = new ConsoleCommandExecutor(this);

		this.players = new ArrayList<Player>();

		this.address = null;
		this.port = 25565;

		ResourceLeakDetector.setLevel(ResourceLeakDetector.Level.DISABLED);
	}

	public void start(OptionSet options) throws Exception {
		this.options = options;
		System.out.println("Setting up logging configuration...");
		ConsoleReader reader = new ConsoleReader();
		System.out.println("Whoop");
		this.consoleReader = new ConsoleReadHandler(this, reader);
		this.consoleWriter = new ConsoleWriteHandler(this, reader, System.out);
		this.consoleReader.start();
		this.consoleWriter.start();

		System.setOut(new PrintStream(new LoggingOutputStream(LogManager.getRootLogger(), Level.INFO), true));
		System.setErr(new PrintStream(new LoggingOutputStream(LogManager.getRootLogger(), Level.ERROR), true));

		LOGGER.info("Starting server...");

		LOGGER.info("Binding server on address {}:{}", address == null ? "*" : address.getHostAddress(), port);
		if (!initServer(address, port)) {
			this.stop("Failed to bind server to the specific address!");
			return;
		}

		LOGGER.info("Starting main server thread...");
		this.serverThread.start();

		LOGGER.info("Server ready for use!");
	}

	private boolean initServer(InetAddress address, int port) {
		boolean epoll = Epoll.isAvailable();
		LOGGER.info("Using {} channel type", epoll ? "epoll" : "default");
		this.eventLoopGroup = epoll ? new EpollEventLoopGroup() : new NioEventLoopGroup();
		try {
			this.bootstrap = new ServerBootstrap();
			bootstrap.group(eventLoopGroup)
					.channel(epoll ? EpollServerSocketChannel.class : NioServerSocketChannel.class)
					.childHandler(serverConnectionListener);

			bootstrap.bind(address, port).syncUninterruptibly();
		} catch (Exception e) {
			LOGGER.fatal("Failed to start the server! Details of the problem below:");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void stop(String reason) {
		LOGGER.info("Stopping server...");
		this.serverThread.stopTicker(reason);
		this.eventLoopGroup.shutdownGracefully(1, 5, TimeUnit.SECONDS).syncUninterruptibly();
		LOGGER.info("Server stopped!");
	}

	public OptionSet getOptions() {
		return options;
	}

	public ServerTicker getTicker() {
		return serverThread;
	}

	public ServerConnectionListener getConnectionListener() {
		return serverConnectionListener;
	}

	public byte getFreeId() {
		for (byte i = 1; i < 256; i++) {
			boolean inUse = false;
			for (Player player : players) {
				if (player.getId() == i) {
					inUse = true;
					break;
				}
			}
			if (!inUse) {
				return i;
			}
		}
		return Player.SELF;
	}

	public String getServerName() {
		return serverName;
	}

	public String getServerMotd() {
		return serverMotd;
	}

	public World getMainWorld() {
		return mainWorld;
	}

	public World getWorld(String name) {
		name = name.toLowerCase();
		for (World world : worlds) {
			if (world.getName().toLowerCase().equals(name)) {
				return world;
			}
		}
		return null;
	}

	public List<World> getWorlds() {
		return worlds;
	}

	public void broadcastMessage(String message) {
		List<String> parts = MessageSanitizer.sanitizeChatMessage(message);
		for (String part : parts) {
			PacketOutMessage packet = new PacketOutMessage(MessageType.CHAT, part);
			for (Player player : players) {
				player.getClientConnection().sendPacket(packet, null);
			}
		}
	}

	public void loginPlayer(Player player) {
		this.players.add(player);
	}

	public List<Player> getPlayers() {
		return Collections.unmodifiableList(players);
	}

	public void logoutPlayer(Player player) {
		this.players.remove(player);
	}

	public Player getPlayer(String name) {
		name = name.toLowerCase();
		for (Player player : players) {
			if (player.getName().toLowerCase().startsWith(name)) {
				return player;
			}
		}
		return null;
	}

	public Player getPlayerExact(String name) {
		name = name.toLowerCase();
		for (Player player : players) {
			if (player.getName().toLowerCase().equals(name)) {
				return player;
			}
		}
		return null;
	}

	public CommandList getCommandList() {
		return commandList;
	}

	public PermissionManager getPermissionManager() {
		return permissionManager;
	}

	public ConsoleCommandExecutor getConsoleCommandExecutor() {
		return consoleCommandExecutor;
	}

	public static Server getInstance() {
		return INSTANCE;
	}
}
